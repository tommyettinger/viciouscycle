package net.myexperiments.viciouscycle;

// JFC
import java.awt.Dimension;
import java.io.IOException;

import net.myexperiments.viciouscycle.controller.VCGOS;

import com.golden.gamedev.GameLoader;

public class ViciousCycle {
	public VCGame vcGame;
	public VCGOS gos;
	GameLoader game;


	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ViciousCycle VC = new ViciousCycle();
		VC.PlayGame();
	}

	void PlayGame() throws IOException {

		// create ViciousCycle game
		vcGame = new VCGame();		
		game = new GameLoader();
		
		game.setup(vcGame, new Dimension(960, 640), false);
		// create logic system

		gos = new VCGOS();		
		gos.InitGOS(vcGame);
		vcGame.gos = gos;
		game.start();

	}




}
