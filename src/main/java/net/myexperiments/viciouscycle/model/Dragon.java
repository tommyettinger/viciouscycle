package net.myexperiments.viciouscycle.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import net.myexperiments.gos.GOSSystem;
import net.myexperiments.gos.World;
import net.myexperiments.gos.agent.AnimateObject;
import net.myexperiments.gos.agent.GameCharacter;
import net.myexperiments.gos.messaging.MailUser;
import net.myexperiments.gos.messaging.Message;
import net.myexperiments.gos.model.InanimateObject;
import net.myexperiments.gos.pathfinder.Node;
import net.myexperiments.gos.schedule.Sleepable;
import net.myexperiments.gos.schedule.Task;
import net.myexperiments.gos.schedule.Sleepable.STATES;
import net.myexperiments.viciouscycle.VCGame;

public class Dragon  implements Sleepable, MailUser{
	
	HashMap<String, Object> characteristics = new HashMap<String, Object>();
	ArrayList<InanimateObject> possessions = new ArrayList<InanimateObject>();
	Task task;
	World world;
	Node loc;
	String name;
	String CharacterClass;
	String Species;
	public Logger Characterlog;
	int number;
	public Dragon(World map, VCGame vcgtge, String name, int number) {
		super();
		world = map;
		this.name = name;
		this.number = number;
		task = new Task(this);
		task.TurnBasedSleep(world.CurrentTurn + 1, STATES.BORN);
		Characterlog = GOSSystem.getLogger();
		world.GOSPS.registerUser(name, this);
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	public void Wakeup(Task t) {
		Characterlog.entering("GameCharacter " + name, "Wakeup", task.getSTATES());
	
	switch (t.STATES) {
	
	case BORN:
		Characterlog.entering("GameCharacter " + name, "BORN");
		// Sleep one second and wake up hungry
		task.RealTimeSleep(1000, STATES.HUNGRY);		
		break;
	case HUNGRY:
		Characterlog.entering("GameCharacter " + name, "HUNGRY");
		// Start exploring next turn
		task.TurnBasedSleep(world.CurrentTurn + 1, STATES.EXPLORING);
		break;
	case DEATH:
		Characterlog.entering("GameCharacter " + name, "DEAD");
		break;
	case EXPLORING:
		Characterlog.entering("GameCharacter " + name, "EXPLORING");
		task.RealTimeSleep(2000, STATES.LIVING);
		break;
	case FIGHTING:
		Characterlog.entering("GameCharacter " + name, "FIGHTING");
		// Fight lasts 3 seconds
		task.RealTimeSleep(3000, STATES.LIVING);
		break;	
	case LIVING:
		Characterlog.entering("GameCharacter " + name, "LIVING");
		task.TurnBasedSleep(world.CurrentTurn+2, STATES.LIVING);
		break;
		}
	}
	

	public void ReceiveMail(Message letter) {

		switch (letter.getType()) {
		
		case BITEME:
			Characterlog.entering(" GameCharacter ", " ReceiveMail ", "From " + letter.getSender() + " To " + name );
			break;
				
		}
		
	}
	@Override
	public World getWorld() {
		// TODO Auto-generated method stub
		return world;
	}
	@Override
	public long getSleeptime() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
