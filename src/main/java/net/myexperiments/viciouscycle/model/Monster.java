package net.myexperiments.viciouscycle.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import net.myexperiments.gos.GOSSystem;
import net.myexperiments.gos.World;
import net.myexperiments.gos.messaging.MailUser;
import net.myexperiments.gos.messaging.Message;
import net.myexperiments.gos.model.InanimateObject;
import net.myexperiments.gos.pathfinder.Node;
import net.myexperiments.gos.schedule.Sleepable;
import net.myexperiments.gos.schedule.Task;
import net.myexperiments.viciouscycle.VCGame;

public class Monster  implements Sleepable, MailUser{
	
	HashMap<String, Object> characteristics = new HashMap<String, Object>();
	ArrayList<InanimateObject> possessions = new ArrayList<InanimateObject>();
	Task task;
	World world;
	Node loc;
	String name;
	String CharacterClass;
	String Species;
	public Logger Characterlog;
	int number;
	public Monster(World map, VCGame vcgtge, String name, int number) {
		super();
		world = map;
		this.name = name;
		this.number = number;
		task = new Task(this);
		task.TurnBasedSleep(world.CurrentTurn + 1, STATES.BORN);
		Characterlog = GOSSystem.getLogger();
		world.GOSPS.registerUser(name, this);
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	public void Wakeup(Task t) {
		Characterlog.entering("Monster " + name, "Wakeup", task.getSTATES());
	
	switch (t.STATES) {
	
	case BORN:
		Characterlog.entering("Monster " + name, "BORN");
		// Sleep one second and wake up hungry
		task.TurnBasedSleep(world.CurrentTurn + 1, STATES.HUNGRY);		
		break;
	case HUNGRY:
		Characterlog.entering("Monster " + name, "HUNGRY");
		// Start exploring next turn
		task.TurnBasedSleep(world.CurrentTurn + 1, STATES.EXPLORING);
		break;
	case DEATH:
		Characterlog.entering("Monster " + name, "DEAD");
		break;
	case EXPLORING:
		Characterlog.entering("Monster " + name, "EXPLORING");
		task.TurnBasedSleep(world.CurrentTurn + 1, STATES.LIVING);
		break;
	case FIGHTING:
		Characterlog.entering("Monster " + name, "FIGHTING");
		// Fight lasts 3 seconds
		task.TurnBasedSleep(world.CurrentTurn + 1, STATES.LIVING);
		break;	
	case LIVING:
		Characterlog.entering("Monster " + name, "LIVING");
		task.TurnBasedSleep(world.CurrentTurn+2, STATES.LIVING);
		break;
		}
	}
	

	public void ReceiveMail(Message letter) {

		switch (letter.getType()) {
		
		case BITEME:
			Characterlog.entering(" Monster ", " ReceiveMail ", "From " + letter.getSender() + " To " + name );
			break;
				
		}
		
	}
	@Override
	public World getWorld() {
		// TODO Auto-generated method stub
		return world;
	}
	@Override
	public long getSleeptime() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
