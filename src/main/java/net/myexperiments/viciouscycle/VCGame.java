package net.myexperiments.viciouscycle;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

import net.myexperiments.gos.World;
import net.myexperiments.viciouscycle.controller.VCGOS;
import net.myexperiments.viciouscycle.model.Dragon;
import net.myexperiments.viciouscycle.model.Monster;
import net.myexperiments.viciouscycle.util.MapMaker;
import net.myexperiments.viciouscycle.util.TSVReader;
import net.myexperiments.viciouscycle.view.Environment;
import net.myexperiments.viciouscycle.view.SlashemNameGenerator;

import com.golden.gamedev.Game;
import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameLoader;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.GameFont;
import com.golden.gamedev.object.PlayField;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.SpriteGroup;
import com.golden.gamedev.object.background.ColorBackground;

/**
 * Comprehensive use of GTGE game engines.
 * 
 * Game class hold all game engines : - Graphics Engine (bsGraphics) - Input
 * Engine (bsInput) - Timer Engine (bsTimer) - Image Engine (bsLoader) - I/O
 * Engine (bsIO) - Sound Engine (bsSound) - Music Engine (bsMusic)
 * 
 * Objective: show the basic use of all engines inside the Game class.
 */
public class VCGame extends Game {
	private static final int MONSTER = 1;
	private static final int DRAGON = 2;
	BufferedImage image;
	// BufferedImage[] images;
	// Map<String, BufferedImage> imagesByName;
	URL url;
	TSVReader tsvr;
	Map<String, Map<String, String>> tableData;
	GameFont font; // bitmap game font

	PlayField playfield;
	Background background;

	SpriteGroup PLAYER_GROUP;

	private Map<String, String> xenomancer, blackguard;
	private Sprite playerSprite, enemySprite;
	private int currentTileX;
	private int currentTileY;
	private Sprite monsterSprite;
	public Environment env;
	public Sprite[] monsterSprites;
	private int playerX;
	private int playerY;
	private Sprite[] monsterSpritesCopy;
	public GameLoader VCgame;
	public VCGOS gos;

	/****************************************************************************/
	/**************************** GAME SKELETON *********************************/
	/****************************************************************************/
	public VCGame() {

	}

	/**
	 * Init game variables.
	 * 
	 * Here we show the basic use of : - Image Engine (bsLoader) - I/O Engine
	 * (bsIO) - Music Engine (bsMusic) - Timer Engine (bsTimer)
	 */
	public void initResources() {
		// OpenGLGameLoader game = new OpenGLGameLoader();
		// game.setupLWJGL(new ViciousCycle(), new Dimension(640,480), false);

		// Image Engine (bsLoader)
		// loading image
		// image = getImage("resources/slashem-revised.png");
		// loading images, 3 column, 1 row
		bsGraphics.setWindowTitle("Vicious Cycle -- Dungeon Demo");

		setFPS(800);

		bsLoader.setMaskColor(new Color(0x476c6c));
		BufferedImage blank;

		// images = bsLoader.getImages("resources/slashem-revised-3.png", 38,
		// 38, true);
		blank = bsLoader.getImage("resources/transparent.png");
		// images = getImages("resources/plane2.png", 3, 1);
		// SlashemNameGenerator sng = new SlashemNameGenerator();
		// imagesByName = sng.readNames(bsLoader, images);

		SlashemNameGenerator.initImages(bsLoader, "resources" + File.separator
				+ "slashem-revised-6.png", "text" + File.separator
				+ "SlashemNamesToIndexes.clj");

		monsterSprites = new Sprite[15];
		monsterSpritesCopy = new Sprite[15];

		// int[][] room2 = MapMaker.randomMap(18, 20);
		int[][] room2 = MapMaker.randomMegaGeomorphsMap(100, 100);

		boolean[][] roomSolids = new boolean[room2.length][room2[0].length];
		for (int i = 0; i < roomSolids.length; i++) {
			for (int j = 0; j < roomSolids[0].length; j++) {
				roomSolids[i][j] = (room2[i][j] != 19);
			}
		}
		env = new Environment(SlashemNameGenerator.getCmapTiles(), room2,
				roomSolids, SlashemNameGenerator.get("cmap.floor of a room"),
				blank, SlashemNameGenerator.get("system.tile.highlight"),
				SlashemNameGenerator.get("system.tile.front"),
				SlashemNameGenerator.get("system.tile.back"), bsInput);
		Point displacement = new Point();
		// background = new SkewedBackground(Arrays.copyOfRange(images, 1175,
		// 1258), room, 48, 32, 16, 0);
		background = new ColorBackground((new Color(0, 30, 100)));
		// background = new TileBackground(Arrays.copyOfRange(images, 1175,
		// 1258), room);
		// background.move(0, 177);
		// background.move(((SkewedBackground)background).getOffsetX(), 0);
		/*
		 * background = new TileBackground(Arrays.copyOfRange(images, 1175,
		 * 1258), new int[][]{ new int[] {3 , 2 , 2 , 4 }, new int[] {1 , 19,
		 * 19, 1 }, new int[] {1 , 19, 19, 1 }, new int[] {1 , 19, 19, 1 }, new
		 * int[] {5 , 2 , 2 , 6 } });
		 */

		// playfield = new PlayField(background);
		// PLAYER_GROUP = playfield.addGroup(new SpriteGroup("Player Group"));
		// TSVReader.convertTSV("text/ViciousCycleClassesAndImages.txt",
		// "text/ViciousCycleClassesAndImages.clj");
		tableData = (TSVReader.readMap("text" + File.separator
				+ "ViciousCycleClassesAndImages.clj"));

		xenomancer = tableData.get("Xenomancer");
		blackguard = tableData.get("Blackguard");
		// playfield = new PlayField(new ColorBackground(new Color(0, 0, 0)));

		currentTileX = this.getRandom(30, 69);
		currentTileY = this.getRandom(30, 69);
		playerSprite = new Sprite(SlashemNameGenerator.get("monster.human or elf.fencer"));
		playerX = currentTileX;
		playerY = currentTileY;
		displacement = env
				.placeFGTile(currentTileX, currentTileY, playerSprite);
		/*
		 * playerSprite.setX(((SkewedBackground)background).getOffsetX(currentTileY
		 * ) + currentTileX * 32);
		 * playerSprite.setY(((SkewedBackground)background
		 * ).getOffsetY(currentTileX) + currentTileY * 32);
		 * PLAYER_GROUP.add(playerSprite);
		 */
		currentTileX = this.getRandom(0, 99);
		currentTileY = this.getRandom(0, 99);
		enemySprite = new Sprite(SlashemNameGenerator.get(blackguard
				.get("Image")));
		/*
		 * enemySprite.setX(((SkewedBackground)background).getOffsetX(currentTileY
		 * ) + currentTileX * 32);
		 * enemySprite.setY(((SkewedBackground)background
		 * ).getOffsetY(currentTileX) + currentTileY * 32);
		 * PLAYER_GROUP.add(enemySprite);
		 */
		displacement = env.placeFGTile(currentTileX, currentTileY, enemySprite);

		// Create characters that live in both GOS and GTGE
		// Always start the same places so errors can be reproduced

		CreateCharacter(MONSTER, "monster.angelic being.baku", 0, 45, 60);
		CreateCharacter(DRAGON, "monster.dragon.shimmering dragon", 1, 14, 84);
		CreateCharacter(DRAGON, "monster.dragon.deep dragon", 2, 4, 6);
		CreateCharacter(DRAGON, "monster.dragon.red dragon", 3, 23, 23);
		CreateCharacter(DRAGON, "monster.dragon.white dragon", 4, 56, 87);
		CreateCharacter(DRAGON, "monster.dragon.chromatic dragon", 5, 67, 34);
		CreateCharacter(DRAGON, "monster.dragon.orange dragon", 6, 12, 17);
		CreateCharacter(DRAGON, "monster.dragon.blue dragon", 7, 87, 56);
		CreateCharacter(DRAGON, "monster.dragon.green dragon", 8, 87, 98);
		CreateCharacter(DRAGON, "monster.dragon.yellow dragon", 9, 28, 56);
		CreateCharacter(MONSTER, "monster.major demon.nalzok", 10, 8, 19);
		CreateCharacter(MONSTER, "monster.major demon.demogorgon", 11, 56, 96);

		// images[38*37]
		/*
		 * monster.dragon.gray dragon monster.dragon.silver dragon
		 * monster.dragon.shimmering dragon monster.dragon.deep dragon
		 * monster.dragon.red dragon monster.dragon.white dragon
		 * monster.dragon.orange dragon monster.dragon.black dragon
		 * monster.dragon.blue dragon monster.dragon.green dragon
		 * monster.dragon.yellow dragon
		 */

		// Music Engine (bsMusic)
		// play midi audio
		// playMusic("resources/music1.mid");

		// Timer Engine (bsTimer)
		// setting game frame-per-second (fps)

		// Game Font Manager
		/*
		 * font = fontManager.getFont(getImages("resources/smallfont.png", 8,
		 * 12), " !\"#$%&'()*+,-./0123456789:;<=>?" +
		 * "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_" +
		 * "`abcdefghijklmnopqrstuvwxyz{|}~~");
		 */
		Font font_denial = new Font("Arial", Font.PLAIN, 16);
		try {
			font_denial = Font.createFont(Font.TRUETYPE_FONT,
					new File("resources/denial.ttf")).deriveFont(Font.PLAIN,
					16f);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		font = fontManager.getFont(font_denial);
		env.scrollInPixels(
				this.getWidth() / 2 + -1 * (int) playerSprite.getX(),
				this.getHeight() / 2 + -1 * (int) playerSprite.getY());
		// env.scrollInTiles(-1 * playerX + playerY / 2, -1 * playerY);
		// env.scrollInTiles(-60, -60);
	}

	/**
	 * Update game variables.
	 * 
	 * Here we show the basic use of: - Input Engine (bsInput) - Sound Engine
	 * (bsSound)
	 */
	public void update(long elapsedTime) {
		// Input Engine (bsInput)
		// poll keyboard and mouse events
		// keyboard event
		// playfield.update(elapsedTime);

		if (keyPressed(KeyEvent.VK_LEFT)) {
			env.scrollInTiles(1, 0);
			// env.update(elapsedTime);

		}
		if (keyPressed(KeyEvent.VK_RIGHT)) {
			env.scrollInTiles(-1, 0);
			// env.update(elapsedTime);

		}
		if (keyPressed(KeyEvent.VK_UP)) {
			env.scrollInTiles(0, 1);
			// env.update(elapsedTime);

		}
		if (keyPressed(KeyEvent.VK_DOWN)) {
			env.scrollInTiles(0, -1);
			// env.update(elapsedTime);

		}
		/*
		 * if (keyDown(KeyEvent.VK_A)) {
		 * System.out.println("A is being pressed"); }
		 * 
		 * // mouse event if (click()) {
		 * System.out.println("Mouse LEFT BUTTON is pressed"); } if
		 * (bsInput.isMouseDown(MouseEvent.BUTTON3)) {
		 * System.out.println("Mouse RIGHT BUTTON is being pressed"); }
		 */

		background.update(elapsedTime);
		env.update(elapsedTime);
		gos.update();
	}

	/**
	 * Render the game to screen.
	 * 
	 * Here we show the basic use of: - Graphics Engine (bsGraphics)
	 */
	public void render(Graphics2D g) {
		// Graphics Engine (bsGraphics)
		// getting window size: getWidth(), getHeight()
		// g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
		// RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		background.render(g);
		env.render(g);
		this.drawFPS(g, 10, 10);
		// playfield.render(g);
		// g.setColor(new Color(0, 150, 170));
		// g.fillRect(0, 0, getWidth(), getHeight());
		//
		// g.setColor(Color.BLACK);
		//
		// g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
		// RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		// font.drawString(g, "====================", 10, 7);
		// font.drawString(g, "Testing Game Engines", 10, 23);
		// font.drawString(g, "====================", 10, 39);
		//
		// font.drawString(g, "Image Engine (bsLoader)", 10, 60);
		// //font.drawString(g,
		// "- Loading \"resource/plane1.png\" and \"resource/plane2.png\" :",
		// 20, 60);
		// //g.drawImage(image, 20, 65, null);
		// g.drawImage(images[0], 80, 85, null);
		// g.drawImage(imagesByName.get(blackguard.get("Image")), 140, 85,
		// null);
		// g.drawImage(imagesByName.get(xenomancer.get("Image")), 220, 85,
		// null);
		// g.drawImage(images[38*37], 300, 85, null);
		// g.drawImage(imagesByName.get(tableData.get("Chaos Mage").get("Image")),
		// 380, 85, null);
		// /*
		// font.drawString(g, "I/O Engine (bsIO)", 10, 145);
		// font.drawString(g, "- Loading file URL \"resources/textfile.txt\" :",
		// 20, 160);
		// font.drawString(g, "  "+url.toString(), 20, 175);
		//
		// font.drawString(g, "Music Engine (bsMusic)", 10, 205);
		// font.drawString(g, "- Playing \"resources/music1.mid\"", 20, 220);
		// */
		// font.drawString(g, "Timer Engine (bsTimer)", 10, 250);
		// font.drawString(g, "- Set FPS to 25", 20, 265);
		// font.drawString(g, "- Get current FPS : "+getCurrentFPS(), 20, 280);
		//
		// font.drawString(g, "Input Engine (bsInput)", 10, 310);
		// font.drawString(g, "- Press \"ENTER\" : print something to console",
		// 20, 325);
		// font.drawString(g, "- Pressing \"A\"  : print something to console",
		// 20, 340);
		// font.drawString(g,
		// "- Click mouse left button     : print something to console", 20,
		// 355);
		// font.drawString(g,
		// "- Clicking mouse right button : print something to console", 20,
		// 370);
		// /*
		// font.drawString(g, "Sound Engine (bsSound)", 10, 400);
		// font.drawString(g,
		// "- Press \"SPACE\" : play \"resources/sound1.wav\"", 20, 415);
		// */
		// //g.drawRect(10, 10, 620, 100);
		//
		// int nexty = font.drawText(g,
		// "Graphics Engine (bsGraphics) : Show this screen, ", GameFont.LEFT,
		// 10, 400, 620, 0, 0);
		// font.drawText(g, "with all this text!", GameFont.LEFT, 10, nexty,
		// 620, 0, 0);
	}

	// Create a GOS and GTGE character

	void CreateCharacter(int tp, String name, int number, int x, int y) {
		monsterSprites[number] = new Sprite(SlashemNameGenerator.get(name));
		monsterSpritesCopy[number] = new Sprite(SlashemNameGenerator.get(name));	
		env.placeFGTile(x, y, monsterSprites[number]);

		switch (tp) {

		case DRAGON:
			Dragon dr = new Dragon(gos.world, this, name, number);
			break;
		case MONSTER:
			Monster mn = new Monster(gos.world, this, name, number);
			break;
		}

	}

	public GameObject getGame(int GameID) {
		// TODO Auto-generated method stub
		return null;
	}
}
