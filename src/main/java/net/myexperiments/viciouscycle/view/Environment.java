package net.myexperiments.viciouscycle.view;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Random;

import com.golden.gamedev.engine.BaseInput;
import com.golden.gamedev.object.Sprite;

public class Environment {
	private Sprite[][][] scene;
	private BufferedImage[] tileImages;
	// private int[][] tiles;
	private Sprite trans;
	private BufferedImage transImage;
	private BaseInput bsInput;
	private int tileX, tileY;
	private Sprite tileCursor, frontCursor, backCursor;
	private int offsetX;
	private int offsetY;

	public Environment(BufferedImage[] tileImages, int[][] tiles,
			boolean[][] whichAreMap, BufferedImage defaultImage,
			BufferedImage blank, BufferedImage tileCursor,
			BufferedImage frontCursor, BufferedImage backCursor, BaseInput bsi) {
		this.bsInput = bsi;
		transImage = blank;
		trans = new Sprite(blank);
		this.tileCursor = new Sprite(tileCursor);
		this.frontCursor = new Sprite(frontCursor);
		this.backCursor = new Sprite(backCursor);
		this.setTileImages(tileImages);
		// this.tiles = tiles;
		if (tiles.length > 0 && tileImages.length > 0) {
			scene = new Sprite[tiles[0].length][tiles.length][5];
			for (int i = 0; i < tiles.length; i++) {
				for (int j = 0; j < tiles[0].length; j++) {
					try {
						/*
						 * if (whichAreMap[i][j]) { scene[j][i][0] = new
						 * Sprite(defaultImage, (j * 32) + ((tiles.length - i) *
						 * 16), i * 32); scene[j][i][1] = new Sprite(blank, (j *
						 * 32) + ((tiles.length - i) * 16), i * 32);
						 * scene[j][i][2] = new Sprite(blank, (j * 32) +
						 * ((tiles.length - i) * 16), i * 32); scene[j][i][3] =
						 * new Sprite( tileImages[tiles[i][j]], (j * 32) +
						 * ((tiles.length - i) * 16), i * 32); scene[j][i][4] =
						 * new Sprite(blank, (j * 32) + ((tiles.length - i) *
						 * 16), i * 32); }
						 */
						if (whichAreMap[i][j]) {
							scene[j][i][0] = new Sprite(defaultImage, offsetX
									+ (j * 32) + ((tiles.length - i) * 16),
									offsetY + i * 32);
							scene[j][i][1] = new Sprite(blank, offsetX
									+ (j * 32) + ((tiles.length - i) * 16),
									offsetY + i * 32);
							scene[j][i][2] = new Sprite(blank, offsetX
									+ (j * 32) + ((tiles.length - i) * 16),
									offsetY + i * 32);
							scene[j][i][3] = new Sprite(
									tileImages[tiles[i][j]], offsetX + (j * 32)
											+ ((tiles.length - i) * 16),
									offsetY + i * 32);
							scene[j][i][4] = new Sprite(blank, offsetX
									+ (j * 32) + ((tiles.length - i) * 16),
									offsetY + i * 32);
						} else {
							scene[j][i][0] = new Sprite(
									tileImages[tiles[i][j]], offsetX + (j * 32)
											+ ((tiles.length - i) * 16),
									offsetY + i * 32);
							scene[j][i][1] = new Sprite(blank, offsetX
									+ (j * 32) + ((tiles.length - i) * 16),
									offsetY + i * 32);
							scene[j][i][2] = new Sprite(blank, offsetX
									+ (j * 32) + ((tiles.length - i) * 16),
									offsetY + i * 32);
							scene[j][i][3] = new Sprite(blank, offsetX
									+ (j * 32) + ((tiles.length - i) * 16),
									offsetY + i * 32);
							scene[j][i][4] = new Sprite(blank, offsetX
									+ (j * 32) + ((tiles.length - i) * 16),
									offsetY + i * 32);
						}
					} catch (ArrayIndexOutOfBoundsException e) {
						scene[j][i][0] = new Sprite(blank, offsetX + (j * 32)
								+ ((tiles.length - i) * 16), offsetY + i * 32);
						scene[j][i][1] = new Sprite(blank, offsetX + (j * 32)
								+ ((tiles.length - i) * 16), offsetY + i * 32);
						scene[j][i][2] = new Sprite(blank, offsetX + (j * 32)
								+ ((tiles.length - i) * 16), offsetY + i * 32);
						scene[j][i][3] = new Sprite(blank, offsetX + (j * 32)
								+ ((tiles.length - i) * 16), offsetY + i * 32);
						scene[j][i][4] = new Sprite(blank, offsetX + (j * 32)
								+ ((tiles.length - i) * 16), offsetY + i * 32);
					}
				}
			}
		} else
			scene = new Sprite[0][0][0];
	}

	public void update(long elapsedTime) {
		try {
			setOverTile(tileX, tileY, trans);
		} catch (ArrayIndexOutOfBoundsException ae) {
		}
		tileX = ((bsInput.getMouseX() - offsetX - ((scene[0].length * 32
				- bsInput.getMouseY() + offsetY) / 2))) / 32 - 1;
		if (tileX < 0)
			tileX = 0;
		if (tileX > scene.length)
			tileX = scene.length;
		tileY = ((bsInput.getMouseY() - offsetY) / 32) - 1;
		if (tileY < 0)
			tileY = 0;
		if (tileY > scene[0].length)
			tileY = scene[0].length;
		try {
			/*
			 * setMidTile(tileX, tileY, backCursor); setOverTile(tileX, tileY,
			 * frontCursor);
			 */
			setMidTile(tileX, tileY, tileCursor);
			setOverTile(tileX, tileY, scene[tileX][tileY][2]);
		} catch (ArrayIndexOutOfBoundsException ae) {
		}
		for (int i = 0; i < scene.length; i++) {
			for (int j = 0; j < scene[0].length; j++) {
				scene[i][j][0].update(elapsedTime);
				scene[i][j][1].update(elapsedTime);
				scene[i][j][2].update(elapsedTime);
				scene[i][j][3].update(elapsedTime);
				scene[i][j][4].update(elapsedTime);
			}
		}
	}

	public void render(Graphics2D g) {
		for (int i = 0; i < scene.length; i++) {
			for (int j = 0; j < scene[0].length; j++) {
				scene[i][j][0].render(g);
				// scene[i][j][1].render(g);
			}
		}
		for (int i = 0; i < scene.length; i++) {
			for (int j = 0; j < scene[0].length; j++) {
				// scene[i][j][0].render(g);
				scene[i][j][1].render(g);
			}
		}
		for (int i = 0; i < scene.length; i++) {
			for (int j = 0; j < scene[0].length; j++) {
				// scene[i][j][0].render(g);
				scene[i][j][2].render(g);
				scene[i][j][3].render(g);
			}
		}/*
		 * for (int i = 0; i < scene.length; i++) { for (int j = 0; j <
		 * scene[0].length; j++) { // scene[i][j][0].render(g); } }
		 */
		for (int i = 0; i < scene.length; i++) {
			for (int j = 0; j < scene[0].length; j++) {
				// scene[i][j][0].render(g);
				scene[i][j][4].render(g);
			}
		}
	}

	public BufferedImage[] getTileImages() {
		return tileImages;
	}

	public void setTileImages(BufferedImage[] tileImages) {
		this.tileImages = tileImages;
	}

	public Sprite getTrans() {
		return trans;
	}

	public void setTrans(Sprite trans) {
		this.trans = trans;
	}

	public void setBGTile(int x, int y, Sprite val) {
		this.scene[x][y][0] = val;
		this.scene[x][y][0].setLocation(offsetX + (x * 32)
				+ ((scene[0].length - y) * 16), offsetY + (y * 32));
	}

	public void setMidTile(int x, int y, Sprite val) {
		this.scene[x][y][1] = val;
		this.scene[x][y][1].setLocation(offsetX + (x * 32)
				+ ((scene[0].length - y) * 16), offsetY + (y * 32));
	}

	public void setFGTile(int x, int y, Sprite val) {
		this.scene[x][y][2] = val;
		this.scene[x][y][2].setLocation(offsetX + (x * 32)
				+ ((scene[0].length - y) * 16), offsetY + (y * 32));
	}

	public void setMapTile(int x, int y, Sprite val) {
		this.scene[x][y][3] = val;
		this.scene[x][y][3].setLocation(offsetX + (x * 32)
				+ ((scene[0].length - y) * 16), offsetY + (y * 32));
	}

	public void setOverTile(int x, int y, Sprite val) {
		this.scene[x][y][4] = val;
		this.scene[x][y][4].setLocation(offsetX + (x * 32)
				+ ((scene[0].length - y) * 16), offsetY + (y * 32));
	}

	public Point placeFGTile(int x, int y, Sprite val) {
		try {
			if (this.scene[x][y][2].getImage().equals(transImage)
					&& this.scene[x][y][3].getImage().equals(transImage)) {
				setFGTile(x, y, val);
				return new Point(0, 0);
			} else {
				for (int i = 3; i > 0; i--) {
					for (int j = 3; j > 0; j--) {
						if (this.scene[x + 1 - (i % 3)][y + 1 - (j % 3)][2]
								.getImage().equals(transImage)
								&& this.scene[x + 1 - (i % 3)][y + 1 - (j % 3)][3]
										.getImage().equals(transImage)) {
							setFGTile(x + 1 - (i % 3), y + 1 - (j % 3), val);
							return new Point(x + 1 - (i % 3), y + 1 - (j % 3));
						}
					}
				}
				for (int i = 5; i > 0; i--) {
					for (int j = 5; j > 0; j--) {
						if (this.scene[x + 2 - (i % 5)][y + 2 - (j % 5)][2]
								.getImage().equals(transImage)
								&& this.scene[x + 2 - (i % 5)][y + 2 - (j % 5)][3]
										.getImage().equals(transImage)) {
							setFGTile(x + 2 - (i % 5), y + 2 - (j % 5), val);

							return new Point(x + 2 - (i % 5), y + 2 - (j % 5));
						}
					}
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		Random rnd = new Random();
		placeFGTile(rnd.nextInt(scene.length), rnd.nextInt(scene[0].length),
				val);
		return new Point(999, 999);
	}

	public void scrollInPixels(int x, int y) {
		// this.update(0);
		offsetX += x;
		offsetY += y;
		// tileCursor.move(32 * x, 32 * y);
		// trans.move(32 * x, 32 * y);
		for (int i = 0; i < scene.length; i++) {
			for (int j = 0; j < scene[i].length; j++) {
				scene[i][j][0].move(x, y);
				scene[i][j][2].move(x, y);
				scene[i][j][3].move(x, y);
			}
		}
	}
	public void scrollInTiles(int x, int y) {
		// this.update(0);
		scrollInPixels(x * 32 - y * 16, y * 32);
		/*offsetX += x * 32 - y * 16;
		offsetY += y * 32;
		// tileCursor.move(32 * x, 32 * y);
		// trans.move(32 * x, 32 * y);
		for (int i = 0; i < scene.length; i++) {
			for (int j = 0; j < scene[i].length; j++) {
				scene[i][j][0].move(32 * x - y * 16, 32 * y);
				scene[i][j][2].move(32 * x - y * 16, 32 * y);
				scene[i][j][3].move(32 * x - y * 16, 32 * y);
			}
		}*/
	}
}
