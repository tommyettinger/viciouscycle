/*
 * Copyright (c) 2008 Golden T Studios.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.myexperiments.viciouscycle.view;

// JFC
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.golden.gamedev.object.background.abstraction.AbstractTileBackground;

/**
 * The basic tiling background, creates a one layer background tile.
 * <p>
 * 
 * <code>TileBackground</code> takes up two parameter, the first one is a two
 * dimensional array of integer (int[][] tiles) that makes up the background
 * tiling, and the second one is the tiling image array (BufferedImage[]
 * tileImages).
 * <p>
 * 
 * This tile background is the basic subclass of
 * <code>AbstractTileBackground</code> that overrides <code>renderTile</code>
 * method to draw one layer tile background :
 * 
 * <pre>
 *    public void render(Graphics2D g, int tileX, int tileY, int x, int y) {
 *       //
 * <code>
 * tiles
 * </code>
 *  is the two dimensional background tiling
 *       int tile = tiles[tileX][tileY];
 *       if (tile &gt;= 0) {
 *          //
 * <code>
 * tileImages
 * </code>
 *  is the tiling images
 *          g.drawImage(tileImages[tile], x, y, null);
 *       }
 *    }
 * </pre>
 * 
 * To create multiple layer, simply subclass <code>AbstractTileBackground</code>
 * and override the <code>renderTile</code> method to render the tile multiple
 * times.
 * <p>
 * 
 * Tile background usage example :
 * 
 * <pre>
 * TileBackground background;
 * BufferedImage[] tileImages;
 * int[][] tiles = new int[40][30]; // 40 x 30 tiling
 * // fill tiles with random value
 * for (int i = 0; i &lt; tiles.length; i++)
 * 	for (int j = 0; j &lt; tiles[0].length; j++)
 * 		tiles[i][j] = getRandom(0, tileImages.length - 1);
 * // create the background
 * background = new TileBackground(tileImages, tiles);
 * </pre>
 * 
 * @see com.golden.gamedev.object.background.abstraction.AbstractTileBackground
 */
public class SkewedBackground extends AbstractTileBackground {

	private static final long serialVersionUID = -837790873922074933L;

	private transient BufferedImage[] tileImages;

	private int[][] tiles;

	private int skewX;

	private int skewY;

	/**
	 * *************************************************************************
	 */
	/**
	 * ***************************** CONSTRUCTOR *******************************
	 */
	/**
	 * *************************************************************************
	 */

	/**
	 * Creates new <code>SkewedBackground</code> with specified tile images,
	 * array of tiles, the width of a tile, the height of a tile, and the X and
	 * Y skew values. The skew value of a tile is the distance from a corner of
	 * the actual image to a corresponding corner of the tile on the grid, for
	 * either the X or Y axis.
	 * <p>
	 * 
	 * The array of tiles that makes up the background tiling, tiles[0][0] = 2
	 * means the tileImages[2] will be drawn on tile 0, 0 coordinate on the map.
	 * 
	 * @param tileImages
	 *            an array of images for the tile
	 * @param tiles
	 *            a two dimensional array that makes up the background
	 * @param tileWidth
	 *            the width of a tile, X of the left corner to the X of the
	 *            right corner.
	 * @param tileHeight
	 *            the height of a tile, Y of the bottom corner to the Y of the
	 *            top corner.
	 * @param skewX
	 *            The X-distance from a corner of the image to the corresponding
	 *            corner of the tile on the grid.
	 * @param skewY
	 *            The Y-distance from a corner of the image to the corresponding
	 *            corner of the tile on the grid.
	 */
	public SkewedBackground(BufferedImage[] tileImages, int[][] tiles,
			int tileWidth, int tileHeight, int skewX, int skewY) {
		super(tiles.length, tiles[0].length, tileWidth - skewX, tileHeight
				- skewY);

		this.tileImages = tileImages;
		this.tiles = tiles;

		this.skewX = skewX;
		this.skewY = skewY;
		
		//this.move(skewX * tiles[0].length, skewY * tiles.length);
		//((Background)this).setSize(this.getWidth() + (skewX * tiles[0].length),
		//this.getHeight() + (skewY * tiles.length));
	}

	/*
	 * Creates new <code>TileBackground</code> with specified tile images, as
	 * big as <code>horiz</code>, <code>vert</code> tiles. <p>
	 * 
	 * Generates tile background with tile as big as horiz and vert (tiles = new
	 * int[horiz][vert]) and using the first image of the tile images
	 * (tileImages[0]) for all the tiles.
	 * 
	 * @param tileImages an array of images for the tile
	 * 
	 * @param horiz total horizontal tiles
	 * 
	 * @param vert total vertical tiles
	 * 
	 * public SkewedBackground(BufferedImage[] tileImages, int horiz, int vert)
	 * { this(tileImages, new int[horiz][vert], tileImages[0].getWidth(),
	 * tileImages[0].getHeight(), 0, 0); }
	 */

	/**
	 * *************************************************************************
	 */
	/**
	 * ************************ RENDER BACKGROUND ******************************
	 */
	/**
	 * *************************************************************************
	 */

	public void renderTile(Graphics2D g, int tileX, int tileY, int x, int y) {
		int tile = this.tiles[tileX][tileY];

		if (tile >= 0) {
			g.drawImage(this.tileImages[tile], (x + (skewX * (tiles[0].length - tileY))),
					(y - (skewY *  (tiles.length - tileX))), null);
			// g.drawString(tileX+","+tileY, x, y-20);
		}
	}

	/**
	 * *************************************************************************
	 */
	/**
	 * ************************* BACKGROUND TILE *******************************
	 */
	/**
	 * *************************************************************************
	 */

	/**
	 * Return the tile background tile images.
	 */
	public BufferedImage[] getTileImages() {
		return this.tileImages;
	}

	/**
	 * Sets the tile background tile images.
	 */
	public void setTileImages(BufferedImage[] tileImages) {
		this.tileImages = tileImages;

		this.setTileSize(tileImages[0].getWidth(), tileImages[0].getHeight());
	}

	/**
	 * Returns the background tiling.
	 */
	public int[][] getTiles() {
		return this.tiles;
	}

	/**
	 * Sets the background tiling.
	 * <p>
	 * 
	 * This array of tiles that makes up the background tiling, tiles[0][0] = 2
	 * means the tileImages[2] will be drawn on tile 0, 0 coordinate on the map.
	 * 
	 * @see #setTileImages(BufferedImage[])
	 */
	public void setTiles(int[][] tiles) {
		this.tiles = tiles;

		super.setSize(tiles.length, tiles[0].length);
	}

	public void setSize(int horiz, int vert) {
		if (horiz != this.tiles.length || vert != this.tiles[0].length) {
			// enlarge/shrink old tiles
			int[][] old = this.tiles;

			this.tiles = new int[horiz][vert];

			int minx = Math.min(this.tiles.length, old.length), miny = Math
					.min(this.tiles[0].length, old[0].length);
			for (int j = 0; j < miny; j++) {
				for (int i = 0; i < minx; i++) {
					this.tiles[i][j] = old[i][j];
				}
			}
		}

		super.setSize(horiz, vert);
	}

	public int getSkewX() {
		return skewX;
	}

	public void setSkewX(int skewX) {
		this.skewX = skewX;
	}

	public int getSkewY() {
		return skewY;
	}

	public void setSkewY(int skewY) {
		this.skewY = skewY;
	}

	public int getOffsetX(int row) {
		return (skewX * (tiles[0].length - row));
	}
	public int getOffsetY(int col) {
		return (skewY * (tiles.length - col));
	}
}
