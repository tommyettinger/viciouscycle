package net.myexperiments.viciouscycle.view;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import net.myexperiments.viciouscycle.util.*;

import com.golden.gamedev.engine.BaseLoader;
   
   
public class SlashemNameGenerator {
	private HashMap<String, BufferedImage> namesToImages;
	public static Map<String, Integer> imageIndexes;
	public static BufferedImage[] allImages;
	public SlashemNameGenerator() {
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, Integer> readIndexes(String filename)
	{
		//namesToImages = new HashMap<String, BufferedImage>(1444);
		imageIndexes = TSVReader.readMap(filename);
		return imageIndexes;
	}
	public static void initImages(BaseLoader bsLoader,
			String imageFile, String indexesFile) {
		bsLoader.setMaskColor(new Color(0x476c6c));
		allImages = bsLoader.getImages(imageFile, 38, 40, true);
		readIndexes(indexesFile);
		//System.out.println(imageIndexes);
	}
	
	public static BufferedImage get(String name)
	{
		return allImages[imageIndexes.get(name)];
	}
	public static BufferedImage[] getCmapTiles()
	{
		return Arrays.copyOfRange(allImages, 1175, 1515);
	}
	public HashMap<String, BufferedImage> readNames(BaseLoader bsLoader,
			BufferedImage[] allImages) {
		namesToImages = new HashMap<String, BufferedImage>(1444);
		bsLoader.setMaskColor(new Color(0x476c6c));
		namesToImages
				.put("monster.ant or other insect.giant ant", allImages[0]);
		namesToImages.put("monster.ant or other insect.giant tick",
				allImages[1]);
		namesToImages.put("monster.ant or other insect.killer bee",
				allImages[2]);
		namesToImages.put("monster.ant or other insect.giant flea",
				allImages[3]);
		namesToImages.put("monster.ant or other insect.soldier ant",
				allImages[4]);
		namesToImages.put("monster.ant or other insect.fire ant", allImages[5]);
		namesToImages.put("monster.ant or other insect.snow ant", allImages[6]);
		namesToImages.put("monster.ant or other insect.giant beetle",
				allImages[7]);
		namesToImages.put("monster.ant or other insect.giant louse",
				allImages[8]);
		namesToImages.put("monster.ant or other insect.tsetse fly",
				allImages[9]);
		namesToImages.put("monster.ant or other insect.migo drone",
				allImages[10]);
		namesToImages.put("monster.ant or other insect.queen bee",
				allImages[11]);
		namesToImages.put("monster.ant or other insect.yellow jacket",
				allImages[12]);
		namesToImages.put("monster.ant or other insect.black wasp",
				allImages[13]);
		namesToImages.put("monster.ant or other insect.migo warrior",
				allImages[14]);
		namesToImages.put("monster.ant or other insect.giant wasp",
				allImages[15]);
		namesToImages.put("monster.ant or other insect.spitting beetle",
				allImages[16]);
		namesToImages.put("monster.ant or other insect.migo queen",
				allImages[17]);
		namesToImages.put("monster.ant or other insect.assassin bug",
				allImages[18]);
		namesToImages.put("monster.ant or other insect.killer beetle",
				allImages[19]);
		namesToImages.put("monster.blob.acid blob", allImages[20]);
		namesToImages.put("monster.blob.quivering blob", allImages[21]);
		namesToImages.put("monster.blob.gelatinous cube", allImages[22]);
		namesToImages.put("monster.blob.jiggling blob", allImages[23]);
		namesToImages.put("monster.blob.lava blob", allImages[24]);
		namesToImages.put("monster.blob.static blob", allImages[25]);
		namesToImages.put("monster.blob.burbling blob", allImages[26]);
		namesToImages.put("monster.cockatrice.chicken", allImages[27]);
		namesToImages.put("monster.cockatrice.cockatoo", allImages[28]);
		namesToImages.put("monster.cockatrice.chickatrice", allImages[29]);
		namesToImages.put("monster.cockatrice.cockatrice", allImages[30]);
		namesToImages.put("monster.cockatrice.pyrolisk", allImages[31]);
		namesToImages.put("monster.cockatrice.parrot", allImages[32]);
		namesToImages.put("monster.dog or other canine.jackal", allImages[33]);
		namesToImages.put("monster.dog or other canine.fox", allImages[34]);
		namesToImages.put("monster.dog or other canine.coyote", allImages[35]);
		namesToImages.put("monster.dog or other canine.werejackal",
				allImages[36]);
		namesToImages.put("monster.dog or other canine.little dog",
				allImages[37]);
		namesToImages.put("monster.dog or other canine.dog", allImages[38]);
		namesToImages.put("monster.dog or other canine.large dog",
				allImages[39]);
		namesToImages
				.put("monster.dog or other canine.pit bull", allImages[40]);
		namesToImages.put("monster.dog or other canine.dingo puppy",
				allImages[41]);
		namesToImages.put("monster.dog or other canine.dingo", allImages[42]);
		namesToImages.put("monster.dog or other canine.large dingo",
				allImages[43]);
		namesToImages.put("monster.dog or other canine.wolf", allImages[44]);
		namesToImages.put("monster.dog or other canine.death dog",
				allImages[45]);
		namesToImages.put("monster.dog or other canine.rabid wolf",
				allImages[46]);
		namesToImages
				.put("monster.dog or other canine.werewolf", allImages[47]);
		namesToImages.put("monster.dog or other canine.warg", allImages[48]);
		namesToImages.put("monster.dog or other canine.winter wolf cub",
				allImages[49]);
		namesToImages.put("monster.dog or other canine.winter wolf",
				allImages[50]);
		namesToImages.put("monster.dog or other canine.hell hound pup",
				allImages[51]);
		namesToImages.put("monster.dog or other canine.wolverine",
				allImages[52]);
		namesToImages.put("monster.dog or other canine.shadow wolf",
				allImages[53]);
		namesToImages.put("monster.dog or other canine.mist wolf",
				allImages[54]);
		namesToImages.put("monster.dog or other canine.hell hound",
				allImages[55]);
		namesToImages
				.put("monster.dog or other canine.cerberus", allImages[56]);
		namesToImages.put("monster.eye or sphere.gas spore", allImages[57]);
		namesToImages.put("monster.eye or sphere.floating eye", allImages[58]);
		namesToImages.put("monster.eye or sphere.glowing eye", allImages[59]);
		namesToImages.put("monster.eye or sphere.freezing sphere",
				allImages[60]);
		namesToImages
				.put("monster.eye or sphere.flaming sphere", allImages[61]);
		namesToImages.put("monster.eye or sphere.shocking sphere",
				allImages[62]);
		namesToImages.put("monster.eye or sphere.bloodshot eye", allImages[63]);
		namesToImages.put("monster.eye or sphere.blinking eye", allImages[64]);
		namesToImages.put("monster.cat or other feline.kitten", allImages[65]);
		namesToImages
				.put("monster.cat or other feline.housecat", allImages[66]);
		namesToImages.put("monster.cat or other feline.jaguar", allImages[67]);
		namesToImages.put("monster.cat or other feline.lynx", allImages[68]);
		namesToImages.put("monster.cat or other feline.panther", allImages[69]);
		namesToImages.put("monster.cat or other feline.werepanther",
				allImages[70]);
		namesToImages.put("monster.cat or other feline.large cat",
				allImages[71]);
		namesToImages.put("monster.cat or other feline.kamadan", allImages[72]);
		namesToImages.put("monster.cat or other feline.displacer beast",
				allImages[73]);
		namesToImages.put("monster.cat or other feline.caterwaul",
				allImages[74]);
		namesToImages.put("monster.cat or other feline.tiger", allImages[75]);
		namesToImages.put("monster.cat or other feline.weretiger",
				allImages[76]);
		namesToImages.put("monster.cat or other feline.sabre-toothed cat",
				allImages[77]);
		namesToImages.put("monster.cat or other feline.hellcat", allImages[78]);
		namesToImages.put("monster.gremlin.gremlin", allImages[79]);
		namesToImages.put("monster.gremlin.gargoyle", allImages[80]);
		namesToImages.put("monster.gremlin.winged gargoyle", allImages[81]);
		namesToImages.put("monster.gremlin.statue gargoyle", allImages[82]);
		namesToImages.put("monster.humanoid.hobbit", allImages[83]);
		namesToImages.put("monster.humanoid.dwarf", allImages[84]);
		namesToImages.put("monster.humanoid.dwarf thief", allImages[85]);
		namesToImages.put("monster.humanoid.bugbear", allImages[86]);
		namesToImages.put("monster.humanoid.dwarf lord", allImages[87]);
		namesToImages.put("monster.humanoid.dwarf king", allImages[88]);
		namesToImages.put("monster.humanoid.duergar", allImages[89]);
		namesToImages.put("monster.humanoid.deep one", allImages[90]);
		namesToImages.put("monster.humanoid.mind flayer", allImages[91]);
		namesToImages.put("monster.humanoid.master mind flayer", allImages[92]);
		namesToImages.put("monster.humanoid.deeper one", allImages[93]);
		namesToImages.put("monster.humanoid.deepest one", allImages[94]);
		namesToImages.put("monster.imp or minor demon.manes", allImages[95]);
		namesToImages.put("monster.imp or minor demon.homunculus",
				allImages[96]);
		namesToImages.put("monster.imp or minor demon.dretch", allImages[97]);
		namesToImages.put("monster.imp or minor demon.imp", allImages[98]);
		namesToImages.put("monster.imp or minor demon.lemure", allImages[99]);
		namesToImages.put("monster.imp or minor demon.quasit", allImages[100]);
		namesToImages.put("monster.imp or minor demon.rutterkin",
				allImages[101]);
		namesToImages.put("monster.imp or minor demon.tengu", allImages[102]);
		namesToImages.put("monster.imp or minor demon.nupperibo",
				allImages[103]);
		namesToImages.put("monster.imp or minor demon.blood imp",
				allImages[104]);
		namesToImages.put("monster.jelly.blue jelly", allImages[105]);
		namesToImages.put("monster.jelly.spotted jelly", allImages[106]);
		namesToImages.put("monster.jelly.clear jelly", allImages[107]);
		namesToImages.put("monster.jelly.ochre jelly", allImages[108]);
		namesToImages.put("monster.jelly.yellow jelly", allImages[109]);
		namesToImages.put("monster.jelly.orange jelly", allImages[110]);
		namesToImages.put("monster.jelly.rancid jelly", allImages[111]);
		namesToImages.put("monster.kobold.kobold", allImages[112]);
		namesToImages.put("monster.kobold.large kobold", allImages[113]);
		namesToImages.put("monster.kobold.kobold lord", allImages[114]);
		namesToImages.put("monster.kobold.kobold shaman", allImages[115]);
		namesToImages.put("monster.kobold.swamp kobold", allImages[116]);
		namesToImages.put("monster.kobold.rock kobold", allImages[117]);
		namesToImages.put("monster.kobold.kobold warrior", allImages[118]);
		namesToImages
				.put("monster.kobold.kroo the kobold king", allImages[119]);
		namesToImages.put("monster.leprechaun.leprechaun", allImages[120]);
		namesToImages.put("monster.leprechaun.leprechaun wizard",
				allImages[121]);
		namesToImages.put("monster.mimic.small mimic", allImages[122]);
		namesToImages.put("monster.mimic.large mimic", allImages[123]);
		namesToImages.put("monster.mimic.giant mimic", allImages[124]);
		namesToImages.put("monster.nymph.wood nymph", allImages[125]);
		namesToImages.put("monster.nymph.water nymph", allImages[126]);
		namesToImages.put("monster.nymph.mountain nymph", allImages[127]);
		namesToImages.put("monster.nymph.pixie", allImages[128]);
		namesToImages.put("monster.nymph.brownie", allImages[129]);
		namesToImages.put("monster.nymph.quickling", allImages[130]);
		namesToImages.put("monster.nymph.aphrodite", allImages[131]);
		namesToImages.put("monster.orc.goblin", allImages[132]);
		namesToImages.put("monster.orc.hobgoblin", allImages[133]);
		namesToImages.put("monster.orc.orc", allImages[134]);
		namesToImages.put("monster.orc.hill orc", allImages[135]);
		namesToImages.put("monster.orc.mordor orc", allImages[136]);
		namesToImages.put("monster.orc.uruk-hai", allImages[137]);
		namesToImages.put("monster.orc.orc shaman", allImages[138]);
		namesToImages.put("monster.orc.orc-captain", allImages[139]);
		namesToImages.put("monster.orc.war orc", allImages[140]);
		namesToImages.put("monster.orc.great orc", allImages[141]);
		namesToImages.put("monster.orc.grund the orc king", allImages[142]);
		namesToImages.put("monster.orc.snow orc", allImages[143]);
		namesToImages.put("monster.orc.demon orc", allImages[144]);
		namesToImages.put("monster.piercer.rock piercer", allImages[145]);
		namesToImages.put("monster.piercer.iron piercer", allImages[146]);
		namesToImages.put("monster.piercer.glass piercer", allImages[147]);
		namesToImages.put("monster.quadruped.lamb", allImages[148]);
		namesToImages.put("monster.quadruped.rothe", allImages[149]);
		namesToImages.put("monster.quadruped.giant badger", allImages[150]);
		namesToImages.put("monster.quadruped.scramper", allImages[151]);
		namesToImages.put("monster.quadruped.sheep", allImages[152]);
		namesToImages.put("monster.quadruped.goat", allImages[153]);
		namesToImages.put("monster.quadruped.squealer", allImages[154]);
		namesToImages.put("monster.quadruped.mumak", allImages[155]);
		namesToImages.put("monster.quadruped.leocrotta", allImages[156]);
		namesToImages.put("monster.quadruped.cow", allImages[157]);
		namesToImages.put("monster.quadruped.mangler", allImages[158]);
		namesToImages.put("monster.quadruped.wumpus", allImages[159]);
		namesToImages.put("monster.quadruped.bull", allImages[160]);
		namesToImages.put("monster.quadruped.titanothere", allImages[161]);
		namesToImages.put("monster.quadruped.baluchitherium", allImages[162]);
		namesToImages.put("monster.quadruped.mastodon", allImages[163]);
		namesToImages.put("monster.quadruped.jumbo the elephant",
				allImages[164]);
		namesToImages.put("monster.quadruped.juggernaut", allImages[165]);
		namesToImages.put("monster.quadruped.catoblepas", allImages[166]);
		namesToImages.put("monster.rodent.sewer rat", allImages[167]);
		namesToImages.put("monster.rodent.rabbit", allImages[168]);
		namesToImages.put("monster.rodent.black rat", allImages[169]);
		namesToImages.put("monster.rodent.giant rat", allImages[170]);
		namesToImages.put("monster.rodent.rabid rat", allImages[171]);
		namesToImages.put("monster.rodent.rabid rabbit", allImages[172]);
		namesToImages.put("monster.rodent.pack rat", allImages[173]);
		namesToImages.put("monster.rodent.wererat", allImages[174]);
		namesToImages.put("monster.rodent.rock mole", allImages[175]);
		namesToImages.put("monster.rodent.woodchuck", allImages[176]);
		namesToImages.put("monster.rodent.hellrat", allImages[177]);
		namesToImages.put("monster.rodent.the rat king", allImages[178]);
		namesToImages.put("monster.spider.cave spider", allImages[179]);
		namesToImages.put("monster.spider.centipede", allImages[180]);
		namesToImages.put("monster.spider.recluse spider", allImages[181]);
		namesToImages.put("monster.spider.giant spider", allImages[182]);
		namesToImages.put("monster.spider.barking spider", allImages[183]);
		namesToImages.put("monster.spider.scorpion", allImages[184]);
		namesToImages.put("monster.spider.carrion crawler", allImages[185]);
		namesToImages.put("monster.spider.nickelpede", allImages[186]);
		namesToImages.put("monster.spider.giant scorpion", allImages[187]);
		namesToImages.put("monster.spider.girtab", allImages[188]);
		namesToImages.put("monster.spider.shelob", allImages[189]);
		namesToImages.put("monster.spider.phase spider", allImages[190]);
		namesToImages.put("monster.spider.werespider", allImages[191]);
		namesToImages.put("monster.trapper or lurker above.lurker above",
				allImages[192]);
		namesToImages.put("monster.trapper or lurker above.trapper",
				allImages[193]);
		namesToImages.put("monster.unicorn or horse.white unicorn",
				allImages[194]);
		namesToImages.put("monster.unicorn or horse.gray unicorn",
				allImages[195]);
		namesToImages.put("monster.unicorn or horse.black unicorn",
				allImages[196]);
		namesToImages.put("monster.unicorn or horse.pony", allImages[197]);
		namesToImages.put("monster.unicorn or horse.horse", allImages[198]);
		namesToImages.put("monster.unicorn or horse.warhorse", allImages[199]);
		namesToImages.put("monster.unicorn or horse.pegasus", allImages[200]);
		namesToImages.put("monster.vortex.fog cloud", allImages[201]);
		namesToImages.put("monster.vortex.dust vortex", allImages[202]);
		namesToImages.put("monster.vortex.ice vortex", allImages[203]);
		namesToImages.put("monster.vortex.energy vortex", allImages[204]);
		namesToImages.put("monster.vortex.steam vortex", allImages[205]);
		namesToImages.put("monster.vortex.fire vortex", allImages[206]);
		namesToImages.put("monster.worm.larva", allImages[207]);
		namesToImages.put("monster.worm.maggot", allImages[208]);
		namesToImages.put("monster.worm.dung worm", allImages[209]);
		namesToImages.put("monster.worm.acid worm", allImages[210]);
		namesToImages.put("monster.worm.bloodworm", allImages[211]);
		namesToImages.put("monster.worm.tunnel worm", allImages[212]);
		namesToImages.put("monster.worm.baby long worm", allImages[213]);
		namesToImages.put("monster.worm.baby purple worm", allImages[214]);
		namesToImages.put("monster.worm.long worm", allImages[215]);
		namesToImages.put("monster.worm.purple worm", allImages[216]);
		namesToImages.put("monster.worm.rot worm", allImages[217]);
		namesToImages.put(
				"monster.xan or other mythical.fantastic insect.grid bug",
				allImages[218]);
		namesToImages.put(
				"monster.xan or other mythical.fantastic insect.spark bug",
				allImages[219]);
		namesToImages.put(
				"monster.xan or other mythical.fantastic insect.arc bug",
				allImages[220]);
		namesToImages.put(
				"monster.xan or other mythical.fantastic insect.lightning bug",
				allImages[221]);
		namesToImages.put("monster.xan or other mythical.fantastic insect.xan",
				allImages[222]);
		namesToImages.put("monster.light.yellow light", allImages[223]);
		namesToImages.put("monster.light.black light", allImages[224]);
		namesToImages.put("unused.001", allImages[225]);
		namesToImages.put("monster.zouthern animal.echidna", allImages[226]);
		namesToImages.put("monster.zouthern animal.platypus", allImages[227]);
		namesToImages.put("monster.zouthern animal.koala", allImages[228]);
		namesToImages.put("monster.zouthern animal.wombat", allImages[229]);
		namesToImages.put("monster.zouthern animal.tasmanian devil",
				allImages[230]);
		namesToImages.put("monster.zouthern animal.wallaby", allImages[231]);
		namesToImages.put("monster.zouthern animal.wallaroo", allImages[232]);
		namesToImages.put("monster.zouthern animal.kangaroo", allImages[233]);
		namesToImages.put("monster.angelic being.couatl", allImages[234]);
		namesToImages.put("monster.angelic being.aleax", allImages[235]);
		namesToImages.put("monster.angelic being.movanic deva", allImages[236]);
		namesToImages.put("monster.angelic being.monadic deva", allImages[237]);
		namesToImages.put("monster.angelic being.astral deva", allImages[238]);
		namesToImages.put("monster.angelic being.angel", allImages[239]);
		namesToImages.put("monster.angelic being.ki-rin", allImages[240]);
		namesToImages.put("monster.angelic being.archon", allImages[241]);
		namesToImages.put("monster.angelic being.planetar", allImages[242]);
		namesToImages.put("monster.angelic being.solar", allImages[243]);
		namesToImages.put("monster.bat or bird.bat", allImages[244]);
		namesToImages.put("monster.bat or bird.giant bat", allImages[245]);
		namesToImages.put("monster.bat or bird.rhumbat", allImages[246]);
		namesToImages.put("monster.bat or bird.athol", allImages[247]);
		namesToImages.put("monster.bat or bird.raven", allImages[248]);
		namesToImages.put("monster.bat or bird.vampire bat", allImages[249]);
		namesToImages.put("monster.bat or bird.hellbat", allImages[250]);
		namesToImages.put("monster.bat or bird.mongbat", allImages[251]);
		namesToImages.put("monster.bat or bird.mobat", allImages[252]);
		namesToImages.put("monster.bat or bird.harpy", allImages[253]);
		namesToImages.put("monster.bat or bird.byakhee", allImages[254]);
		namesToImages.put("monster.bat or bird.nightgaunt", allImages[255]);
		namesToImages.put("monster.centaur.plains centaur", allImages[256]);
		namesToImages.put("monster.centaur.forest centaur", allImages[257]);
		namesToImages.put("monster.centaur.mountain centaur", allImages[258]);
		namesToImages.put("monster.dragon.baby gray dragon", allImages[259]);
		namesToImages.put("monster.dragon.baby silver dragon", allImages[260]);
		namesToImages.put("monster.dragon.baby shimmering dragon",
				allImages[261]);
		namesToImages.put("monster.dragon.baby deep dragon", allImages[262]);
		namesToImages.put("monster.dragon.baby red dragon", allImages[263]);
		namesToImages.put("monster.dragon.baby white dragon", allImages[264]);
		namesToImages.put("monster.dragon.baby orange dragon", allImages[265]);
		namesToImages.put("monster.dragon.baby black dragon", allImages[266]);
		namesToImages.put("monster.dragon.baby blue dragon", allImages[267]);
		namesToImages.put("monster.dragon.baby green dragon", allImages[268]);
		namesToImages.put("monster.dragon.baby yellow dragon", allImages[269]);
		namesToImages.put("monster.dragon.gray dragon", allImages[270]);
		namesToImages.put("monster.dragon.silver dragon", allImages[271]);
		namesToImages.put("monster.dragon.shimmering dragon", allImages[272]);
		namesToImages.put("monster.dragon.deep dragon", allImages[273]);
		namesToImages.put("monster.dragon.red dragon", allImages[274]);
		namesToImages.put("monster.dragon.white dragon", allImages[275]);
		namesToImages.put("monster.dragon.orange dragon", allImages[276]);
		namesToImages.put("monster.dragon.black dragon", allImages[277]);
		namesToImages.put("monster.dragon.blue dragon", allImages[278]);
		namesToImages.put("monster.dragon.green dragon", allImages[279]);
		namesToImages.put("monster.dragon.yellow dragon", allImages[280]);
		namesToImages.put("monster.dragon.wyvern", allImages[281]);
		namesToImages.put("monster.dragon.hydra", allImages[282]);
		namesToImages.put("monster.elemental.stalker", allImages[283]);
		namesToImages.put("monster.elemental.air elemental", allImages[284]);
		namesToImages.put("monster.elemental.fire elemental", allImages[285]);
		namesToImages.put("monster.elemental.earth elemental", allImages[286]);
		namesToImages.put("monster.elemental.water elemental", allImages[287]);
		namesToImages.put("monster.fungus or mold.lichen", allImages[288]);
		namesToImages.put("monster.fungus or mold.brown mold", allImages[289]);
		namesToImages.put("monster.fungus or mold.yellow mold", allImages[290]);
		namesToImages.put("monster.fungus or mold.green mold", allImages[291]);
		namesToImages.put("monster.fungus or mold.red mold", allImages[292]);
		namesToImages.put("monster.fungus or mold.shrieker", allImages[293]);
		namesToImages.put("monster.fungus or mold.violet fungus",
				allImages[294]);
		namesToImages.put("monster.fungus or mold.disgusting mold",
				allImages[295]);
		namesToImages.put("monster.fungus or mold.black mold", allImages[296]);
		namesToImages.put("monster.gnome.gnome", allImages[297]);
		namesToImages.put("monster.gnome.gnome thief", allImages[298]);
		namesToImages.put("monster.gnome.gnome lord", allImages[299]);
		namesToImages.put("monster.gnome.gnomish wizard", allImages[300]);
		namesToImages.put("monster.gnome.deep gnome", allImages[301]);
		namesToImages.put("monster.gnome.gnome warrior", allImages[302]);
		namesToImages.put("monster.gnome.ruggo the gnome king", allImages[303]);
		namesToImages.put("monster.gnome.gnome king", allImages[304]);
		namesToImages.put("monster.gnome.gnoll", allImages[305]);
		namesToImages.put("monster.gnome.gnoll warrior", allImages[306]);
		namesToImages.put("monster.gnome.gnoll chieftain", allImages[307]);
		namesToImages.put("monster.gnome.gnoll shaman", allImages[308]);
		namesToImages.put("monster.giant humanoid.giant", allImages[309]);
		namesToImages.put("monster.giant humanoid.stone giant", allImages[310]);
		namesToImages.put("monster.giant humanoid.hill giant", allImages[311]);
		namesToImages.put("monster.giant humanoid.fire giant", allImages[312]);
		namesToImages.put("monster.giant humanoid.frost giant", allImages[313]);
		namesToImages.put("monster.giant humanoid.ettin", allImages[314]);
		namesToImages.put("monster.giant humanoid.titan", allImages[315]);
		namesToImages.put("monster.giant humanoid.storm giant", allImages[316]);
		namesToImages.put("monster.giant humanoid.minotaur", allImages[317]);
		namesToImages.put("monster.giant humanoid.the largest giant",
				allImages[318]);
		namesToImages
				.put("monster.giant humanoid.father dagon", allImages[319]);
		namesToImages
				.put("monster.giant humanoid.mother hydra", allImages[320]);
		namesToImages.put("monster.jabberwock.jabberwock", allImages[321]);
		namesToImages.put("monster.jabberwock.vorpal jabberwock",
				allImages[322]);
		namesToImages.put("monster.keystone kop.keystone kop", allImages[323]);
		namesToImages.put("monster.keystone kop.kop sergeant", allImages[324]);
		namesToImages
				.put("monster.keystone kop.kop lieutenant", allImages[325]);
		namesToImages.put("monster.keystone kop.kop kaptain", allImages[326]);
		namesToImages.put("monster.lich.lich", allImages[327]);
		namesToImages.put("monster.lich.demilich", allImages[328]);
		namesToImages.put("monster.lich.master lich", allImages[329]);
		namesToImages.put("monster.lich.arch-lich", allImages[330]);
		namesToImages.put("monster.mummy.kobold mummy", allImages[331]);
		namesToImages.put("monster.mummy.gnome mummy", allImages[332]);
		namesToImages.put("monster.mummy.orc mummy", allImages[333]);
		namesToImages.put("monster.mummy.dwarf mummy", allImages[334]);
		namesToImages.put("monster.mummy.elf mummy", allImages[335]);
		namesToImages.put("monster.mummy.human mummy", allImages[336]);
		namesToImages.put("monster.mummy.ettin mummy", allImages[337]);
		namesToImages.put("monster.mummy.giant mummy", allImages[338]);
		namesToImages.put("monster.mummy.troll mummy", allImages[339]);
		namesToImages.put("monster.naga.red naga hatchling", allImages[340]);
		namesToImages.put("monster.naga.black naga hatchling", allImages[341]);
		namesToImages.put("monster.naga.golden naga hatchling", allImages[342]);
		namesToImages.put("monster.naga.guardian naga hatchling",
				allImages[343]);
		namesToImages.put("monster.naga.red naga", allImages[344]);
		namesToImages.put("monster.naga.black naga", allImages[345]);
		namesToImages.put("monster.naga.golden naga", allImages[346]);
		namesToImages.put("monster.naga.guardian naga", allImages[347]);
		namesToImages.put("monster.ogre.ogre", allImages[348]);
		namesToImages.put("monster.ogre.ogre lord", allImages[349]);
		namesToImages.put("monster.ogre.ogre mage", allImages[350]);
		namesToImages.put("monster.ogre.ogre king", allImages[351]);
		namesToImages.put("monster.ogre.shadow ogre", allImages[352]);
		namesToImages.put("monster.pudding or ooze.gray ooze", allImages[353]);
		namesToImages.put("monster.pudding or ooze.brown pudding",
				allImages[354]);
		namesToImages.put("monster.pudding or ooze.moldy pudding",
				allImages[355]);
		namesToImages.put("monster.pudding or ooze.black pudding",
				allImages[356]);
		namesToImages
				.put("monster.pudding or ooze.green slime", allImages[357]);
		namesToImages.put("monster.pudding or ooze.shoggoth", allImages[358]);
		namesToImages.put("monster.pudding or ooze.giant shoggoth",
				allImages[359]);
		namesToImages.put(
				"monster.quantum mechanic or other scientist.quantum mechanic",
				allImages[360]);
		namesToImages.put(
				"monster.quantum mechanic or other scientist.genetic engineer",
				allImages[361]);
		namesToImages
				.put("monster.quantum mechanic or other scientist.doctor frankenstein",
						allImages[362]);
		namesToImages.put("monster.rust monster or disenchanter.rust monster",
				allImages[363]);
		namesToImages.put("monster.rust monster or disenchanter.disenchanter",
				allImages[364]);
		namesToImages.put("monster.snake.garter snake", allImages[365]);
		namesToImages.put("monster.snake.snake", allImages[366]);
		namesToImages.put("monster.snake.water moccasin", allImages[367]);
		namesToImages.put("monster.snake.pit viper", allImages[368]);
		namesToImages.put("monster.snake.python", allImages[369]);
		namesToImages.put("monster.snake.cobra", allImages[370]);
		namesToImages.put("monster.snake.king cobra", allImages[371]);
		namesToImages.put("monster.snake.weresnake", allImages[372]);
		namesToImages.put("monster.snake.asphynx", allImages[373]);
		namesToImages.put("monster.troll.troll", allImages[374]);
		namesToImages.put("monster.troll.ice troll", allImages[375]);
		namesToImages.put("monster.troll.rock troll", allImages[376]);
		namesToImages.put("monster.troll.two-headed troll", allImages[377]);
		namesToImages.put("monster.troll.water troll", allImages[378]);
		namesToImages.put("monster.troll.olog-hai", allImages[379]);
		namesToImages.put("monster.troll.black troll", allImages[380]);
		namesToImages.put("monster.umber hulk.umber hulk", allImages[381]);
		namesToImages.put("monster.umber hulk.water hulk", allImages[382]);
		namesToImages.put("monster.vampire.vampire", allImages[383]);
		namesToImages.put("monster.vampire.vampire lord", allImages[384]);
		namesToImages.put("monster.vampire.fire vampire", allImages[385]);
		namesToImages.put("monster.vampire.star vampire", allImages[386]);
		namesToImages.put("monster.vampire.vampire mage", allImages[387]);
		namesToImages.put("monster.vampire.vlad the impaler", allImages[388]);
		namesToImages.put("monster.wraith.barrow wight", allImages[389]);
		namesToImages.put("monster.wraith.wight", allImages[390]);
		namesToImages.put("monster.wraith.wraith", allImages[391]);
		namesToImages.put("monster.wraith.nazgul", allImages[392]);
		namesToImages.put("monster.xorn.xorn", allImages[393]);
		namesToImages.put("monster.yeti ape or other large beast.monkey",
				allImages[394]);
		namesToImages.put("monster.yeti ape or other large beast.ape",
				allImages[395]);
		namesToImages.put("monster.yeti ape or other large beast.owlbear",
				allImages[396]);
		namesToImages.put("monster.yeti ape or other large beast.yeti",
				allImages[397]);
		namesToImages.put(
				"monster.yeti ape or other large beast.carnivorous ape",
				allImages[398]);
		namesToImages.put("monster.yeti ape or other large beast.sasquatch",
				allImages[399]);
		namesToImages.put("monster.yeti ape or other large beast.zruty",
				allImages[400]);
		namesToImages.put("monster.zombie.kobold zombie", allImages[401]);
		namesToImages.put("monster.zombie.gnome zombie", allImages[402]);
		namesToImages.put("monster.zombie.orc zombie", allImages[403]);
		namesToImages.put("monster.zombie.dwarf zombie", allImages[404]);
		namesToImages.put("monster.zombie.elf zombie", allImages[405]);
		namesToImages.put("monster.zombie.human zombie", allImages[406]);
		namesToImages.put("monster.zombie.ghoul", allImages[407]);
		namesToImages.put("monster.zombie.ghoul mage", allImages[408]);
		namesToImages.put("monster.zombie.ettin zombie", allImages[409]);
		namesToImages.put("monster.zombie.ghast", allImages[410]);
		namesToImages.put("monster.zombie.giant zombie", allImages[411]);
		namesToImages.put("monster.zombie.skeleton", allImages[412]);
		namesToImages.put("monster.zombie.ghoul queen", allImages[413]);
		namesToImages.put("monster.zombie.gug", allImages[414]);
		namesToImages.put("monster.golem.straw golem", allImages[415]);
		namesToImages.put("monster.golem.paper golem", allImages[416]);
		namesToImages.put("monster.golem.wax golem", allImages[417]);
		namesToImages.put("monster.golem.plastic golem", allImages[418]);
		namesToImages.put("monster.golem.rope golem", allImages[419]);
		namesToImages.put("monster.golem.gold golem", allImages[420]);
		namesToImages.put("monster.golem.leather golem", allImages[421]);
		namesToImages.put("monster.golem.wood golem", allImages[422]);
		namesToImages.put("monster.golem.flesh golem", allImages[423]);
		namesToImages.put("monster.golem.clay golem", allImages[424]);
		namesToImages.put("monster.golem.stone golem", allImages[425]);
		namesToImages.put("monster.golem.glass golem", allImages[426]);
		namesToImages.put("monster.golem.iron golem", allImages[427]);
		namesToImages
				.put("monster.golem.frankensteins monster", allImages[428]);
		namesToImages.put("monster.golem.ruby golem", allImages[429]);
		namesToImages.put("monster.golem.diamond golem", allImages[430]);
		namesToImages.put("monster.golem.sapphire golem", allImages[431]);
		namesToImages.put("monster.golem.steel golem", allImages[432]);
		namesToImages.put("monster.golem.crystal golem", allImages[433]);
		namesToImages.put("monster.human or elf.human", allImages[434]);
		namesToImages.put("monster.human or elf.wererat", allImages[435]);
		namesToImages.put("monster.human or elf.werejackal", allImages[436]);
		namesToImages.put("monster.human or elf.werewolf", allImages[437]);
		namesToImages.put("monster.human or elf.werepanther", allImages[438]);
		namesToImages.put("monster.human or elf.weretiger", allImages[439]);
		namesToImages.put("monster.human or elf.weresnake", allImages[440]);
		namesToImages.put("monster.human or elf.werespider", allImages[441]);
		namesToImages.put("monster.human or elf.gibberling", allImages[442]);
		namesToImages.put("monster.human or elf.grimlock", allImages[443]);
		namesToImages.put("monster.human or elf.elf", allImages[444]);
		namesToImages.put("monster.human or elf.woodland-elf", allImages[445]);
		namesToImages.put("monster.human or elf.green-elf", allImages[446]);
		namesToImages.put("monster.human or elf.grey-elf", allImages[447]);
		namesToImages.put("monster.human or elf.high-elf", allImages[448]);
		namesToImages.put("monster.human or elf.elf-lord", allImages[449]);
		namesToImages.put("monster.human or elf.drow", allImages[450]);
		namesToImages.put("monster.human or elf.elvenking", allImages[451]);
		namesToImages.put("monster.human or elf.doppelganger", allImages[452]);
		namesToImages.put("monster.human or elf.mugger", allImages[453]);
		namesToImages.put("monster.human or elf.nurse", allImages[454]);
		namesToImages.put("monster.human or elf.gypsy", allImages[455]);
		namesToImages.put("monster.human or elf.shopkeeper", allImages[456]);
		namesToImages.put("monster.human or elf.black marketeer",
				allImages[457]);
		namesToImages.put("monster.human or elf.guard", allImages[458]);
		namesToImages.put("monster.human or elf.prisoner", allImages[459]);
		namesToImages.put("monster.human or elf.oracle", allImages[460]);
		namesToImages
				.put("monster.human or elf.aligned priest", allImages[461]);
		namesToImages.put("monster.human or elf.high priest", allImages[462]);
		namesToImages.put("monster.human or elf.soldier", allImages[463]);
		namesToImages.put("monster.human or elf.sergeant", allImages[464]);
		namesToImages.put("monster.human or elf.lieutenant", allImages[465]);
		namesToImages.put("monster.human or elf.captain", allImages[466]);
		namesToImages.put("monster.human or elf.watchman", allImages[467]);
		namesToImages.put("monster.human or elf.watch captain", allImages[468]);
		namesToImages.put("monster.human or elf.medusa", allImages[469]);
		namesToImages.put("monster.human or elf.wizard of yendor",
				allImages[470]);
		namesToImages.put("monster.human or elf.croesus", allImages[471]);
		namesToImages.put("monster.human or elf.charon", allImages[472]);
		namesToImages.put("monster.ghost.shadow", allImages[473]);
		namesToImages.put("monster.ghost.ghost", allImages[474]);
		namesToImages.put("monster.ghost.shade", allImages[475]);
		namesToImages.put("monster.major demon.water demon", allImages[476]);
		namesToImages.put("monster.major demon.horned devil", allImages[477]);
		namesToImages.put("monster.major demon.spined devil", allImages[478]);
		namesToImages.put("monster.major demon.bearded devil", allImages[479]);
		namesToImages.put("monster.major demon.succubus", allImages[480]);
		namesToImages.put("monster.major demon.incubus", allImages[481]);
		namesToImages.put("monster.major demon.erinys", allImages[482]);
		namesToImages.put("monster.major demon.barbed devil", allImages[483]);
		namesToImages.put("monster.major demon.marilith", allImages[484]);
		namesToImages.put("monster.major demon.bar-lgura", allImages[485]);
		namesToImages.put("monster.major demon.chasme", allImages[486]);
		namesToImages.put("monster.major demon.vrock", allImages[487]);
		namesToImages.put("monster.major demon.babau", allImages[488]);
		namesToImages.put("monster.major demon.hezrou", allImages[489]);
		namesToImages.put("monster.major demon.bone devil", allImages[490]);
		namesToImages.put("monster.major demon.ice devil", allImages[491]);
		namesToImages.put("monster.major demon.nalfeshnee", allImages[492]);
		namesToImages.put("monster.major demon.nabassu", allImages[493]);
		namesToImages.put("monster.major demon.pit fiend", allImages[494]);
		namesToImages.put("monster.major demon.balrog", allImages[495]);
		namesToImages.put("monster.major demon.juiblex", allImages[496]);
		namesToImages.put("monster.major demon.yeenoghu", allImages[497]);
		namesToImages.put("monster.major demon.orcus", allImages[498]);
		namesToImages.put("monster.major demon.geryon", allImages[499]);
		namesToImages.put("monster.major demon.dispater", allImages[500]);
		namesToImages.put("monster.major demon.baalzebub", allImages[501]);
		namesToImages.put("monster.major demon.asmodeus", allImages[502]);
		namesToImages.put("monster.major demon.demogorgon", allImages[503]);
		namesToImages.put("monster.major demon.cthulhu", allImages[504]);
		namesToImages.put("monster.major demon.death", allImages[505]);
		namesToImages.put("monster.major demon.pestilence", allImages[506]);
		namesToImages.put("monster.major demon.famine", allImages[507]);
		namesToImages.put("monster.major demon.mail daemon", allImages[508]);
		namesToImages.put("monster.major demon.djinni", allImages[509]);
		namesToImages.put("monster.major demon.efreeti", allImages[510]);
		namesToImages.put("monster.major demon.dao", allImages[511]);
		namesToImages.put("monster.major demon.marid", allImages[512]);
		namesToImages.put("monster.major demon.sandestin", allImages[513]);
		namesToImages.put("monster.sea monster.jellyfish", allImages[514]);
		namesToImages.put("monster.sea monster.piranha", allImages[515]);
		namesToImages.put("monster.sea monster.giant eel", allImages[516]);
		namesToImages.put("monster.sea monster.shark", allImages[517]);
		namesToImages.put("monster.sea monster.giant crab", allImages[518]);
		namesToImages.put("monster.sea monster.electric eel", allImages[519]);
		namesToImages.put("monster.sea monster.kraken", allImages[520]);
		namesToImages.put("monster.lizard.newt", allImages[521]);
		namesToImages.put("monster.lizard.gecko", allImages[522]);
		namesToImages.put("monster.lizard.iguana", allImages[523]);
		namesToImages.put("monster.lizard.baby crocodile", allImages[524]);
		namesToImages.put("monster.lizard.lizard", allImages[525]);
		namesToImages.put("monster.lizard.gila monster", allImages[526]);
		namesToImages.put("monster.lizard.chameleon", allImages[527]);
		namesToImages.put("monster.lizard.crocodile", allImages[528]);
		namesToImages.put("monster.lizard.salamander", allImages[529]);
		namesToImages.put("monster.lizard.rhaumbusun", allImages[530]);
		namesToImages.put("monster.lizard.basilisk", allImages[531]);
		namesToImages.put("monster.lizard.komodo dragon", allImages[532]);
		namesToImages.put("monster.piece of food.bad egg", allImages[533]);
		namesToImages.put("monster.piece of food.killer tripe ration",
				allImages[534]);
		namesToImages.put("monster.piece of food.killer food ration",
				allImages[535]);
		namesToImages.put("monster.pile of coins.pile of killer coins",
				allImages[536]);
		namesToImages.put("monster.pile of coins.large pile of killer coins",
				allImages[537]);
		namesToImages.put("monster.pile of coins.huge pile of killer coins",
				allImages[538]);
		namesToImages.put("monster.long worm tail.long worm tail",
				allImages[539]);
		namesToImages.put("monster.unicorn or horse.nightmare", allImages[540]);
		namesToImages.put("monster.eye or sphere.beholder", allImages[541]);
		namesToImages.put("monster.lich.vecna", allImages[542]);
		namesToImages.put("monster.human or elf.archeologist", allImages[543]);
		namesToImages.put("monster.human or elf.barbarian", allImages[544]);
		namesToImages.put("monster.human or elf.caveman", allImages[545]);
		namesToImages.put("monster.human or elf.cavewoman", allImages[546]);
		namesToImages.put("monster.human or elf.flame mage", allImages[547]);
		namesToImages.put("monster.human or elf.healer", allImages[548]);
		namesToImages.put("monster.human or elf.ice mage", allImages[549]);
		namesToImages.put("monster.human or elf.knight", allImages[550]);
		namesToImages.put("monster.human or elf.monk", allImages[551]);
		namesToImages.put("monster.human or elf.necromancer", allImages[552]);
		namesToImages.put("monster.human or elf.priest", allImages[553]);
		namesToImages.put("monster.human or elf.priestess", allImages[554]);
		namesToImages.put("monster.human or elf.ranger", allImages[555]);
		namesToImages.put("monster.human or elf.rogue", allImages[556]);
		namesToImages.put("monster.human or elf.samurai", allImages[557]);
		namesToImages.put("monster.human or elf.tourist", allImages[558]);
		namesToImages.put("monster.human or elf.undead slayer", allImages[559]);
		namesToImages.put("monster.human or elf.valkyrie", allImages[560]);
		namesToImages.put("monster.human or elf.yeoman", allImages[561]);
		namesToImages.put("monster.human or elf.wizard", allImages[562]);
		namesToImages
				.put("monster.human or elf.lord carnarvon", allImages[563]);
		namesToImages.put("monster.human or elf.pelias", allImages[564]);
		namesToImages.put("monster.human or elf.shaman karnov", allImages[565]);
		namesToImages
				.put("monster.human or elf.master shifter", allImages[566]);
		namesToImages.put("monster.humanoid.thorin", allImages[567]);
		namesToImages.put("monster.human or elf.earendil", allImages[568]);
		namesToImages.put("monster.human or elf.elwing", allImages[569]);
		namesToImages.put("monster.human or elf.high flame mage",
				allImages[570]);
		namesToImages.put("monster.human or elf.hippocrates", allImages[571]);
		namesToImages.put("monster.humanoid.bilbo baggins", allImages[572]);
		namesToImages.put("monster.human or elf.high ice mage", allImages[573]);
		namesToImages.put("monster.human or elf.king arthur", allImages[574]);
		namesToImages.put("monster.human or elf.high lycanthrope",
				allImages[575]);
		namesToImages.put("monster.human or elf.grand master", allImages[576]);
		namesToImages.put("monster.human or elf.dark lord", allImages[577]);
		namesToImages.put("monster.human or elf.arch priest", allImages[578]);
		namesToImages.put("monster.human or elf.orion", allImages[579]);
		namesToImages.put("monster.human or elf.master of thieves",
				allImages[580]);
		namesToImages.put("monster.human or elf.lord sato", allImages[581]);
		namesToImages.put("monster.human or elf.twoflower", allImages[582]);
		namesToImages.put("monster.human or elf.van helsing", allImages[583]);
		namesToImages.put("monster.human or elf.norn", allImages[584]);
		namesToImages.put("monster.human or elf.neferet the green",
				allImages[585]);
		namesToImages.put("monster.spider.lolth", allImages[586]);
		namesToImages.put("monster.human or elf.chief yeoman warder",
				allImages[587]);
		namesToImages.put("monster.major demon.minion of huhetotl",
				allImages[588]);
		namesToImages.put("monster.human or elf.thoth amon", allImages[589]);
		namesToImages.put("monster.dragon.chromatic dragon", allImages[590]);
		namesToImages.put("monster.human or elf.transmuter", allImages[591]);
		namesToImages.put("monster.dragon.smaug", allImages[592]);
		namesToImages.put("monster.orc.goblin king", allImages[593]);
		namesToImages.put("monster.human or elf.water mage", allImages[594]);
		namesToImages.put("monster.human or elf.lareth", allImages[595]);
		namesToImages.put("monster.giant humanoid.cyclops", allImages[596]);
		namesToImages.put("monster.humanoid.gollum", allImages[597]);
		namesToImages.put("monster.human or elf.earth mage", allImages[598]);
		namesToImages.put("monster.dragon.ixoth", allImages[599]);
		namesToImages.put("monster.human or elf.sir lorimar", allImages[600]);
		namesToImages.put("monster.human or elf.master kaen", allImages[601]);
		namesToImages.put("monster.major demon.maugneshaagar", allImages[602]);
		namesToImages.put("monster.major demon.nalzok", allImages[603]);
		namesToImages.put("monster.spider.scorpius", allImages[604]);
		namesToImages.put("monster.human or elf.master assassin",
				allImages[605]);
		namesToImages.put("monster.human or elf.ashikaga takauji",
				allImages[606]);
		namesToImages.put("monster.human or elf.count dracula", allImages[607]);
		namesToImages.put("monster.giant humanoid.lord surtur", allImages[608]);
		namesToImages.put("monster.human or elf.dark one", allImages[609]);
		namesToImages.put("monster.human or elf.colonel blood", allImages[610]);
		namesToImages.put("monster.human or elf.student", allImages[611]);
		namesToImages.put("monster.human or elf.chieftain", allImages[612]);
		namesToImages.put("monster.human or elf.neanderthal", allImages[613]);
		namesToImages.put("monster.human or elf.shifter", allImages[614]);
		namesToImages.put("monster.humanoid.dwarf warrior", allImages[615]);
		namesToImages.put("monster.human or elf.igniter", allImages[616]);
		namesToImages.put("monster.human or elf.froster", allImages[617]);
		namesToImages.put("monster.humanoid.fiend", allImages[618]);
		namesToImages.put("monster.human or elf.attendant", allImages[619]);
		namesToImages.put("monster.humanoid.proudfoot", allImages[620]);
		namesToImages.put("monster.human or elf.intern", allImages[621]);
		namesToImages.put("monster.human or elf.page", allImages[622]);
		namesToImages.put("monster.human or elf.abbot", allImages[623]);
		namesToImages.put("monster.human or elf.embalmer", allImages[624]);
		namesToImages.put("monster.human or elf.acolyte", allImages[625]);
		namesToImages.put("monster.human or elf.hunter", allImages[626]);
		namesToImages.put("monster.human or elf.thug", allImages[627]);
		namesToImages.put("monster.human or elf.ninja", allImages[628]);
		namesToImages.put("monster.human or elf.ronin", allImages[629]);
		namesToImages.put("monster.human or elf.roshi", allImages[630]);
		namesToImages.put("monster.human or elf.guide", allImages[631]);
		namesToImages.put("monster.human or elf.exterminator", allImages[632]);
		namesToImages.put("monster.human or elf.warrior", allImages[633]);
		namesToImages.put("monster.human or elf.apprentice", allImages[634]);
		namesToImages.put("monster.human or elf.yeoman warder", allImages[635]);
		namesToImages.put("monster.humanoid.farmer maggot", allImages[636]);
		namesToImages.put("monster.invisible monster.invisible monster",
				allImages[637]);
		namesToImages.put("object.illegal objects.strange object",
				allImages[638]);
		namesToImages.put("object.weapons.crude dagger.orcish dagger",
				allImages[639]);
		namesToImages.put("object.weapons.dagger", allImages[640]);
		namesToImages.put("object.weapons.athame", allImages[641]);
		namesToImages.put("object.weapons.silver dagger", allImages[642]);
		namesToImages.put("object.weapons.runed dagger.elven dagger",
				allImages[643]);
		namesToImages.put(
				"object.weapons.black runed dagger.dark elven dagger",
				allImages[644]);
		namesToImages.put("object.weapons.wooden stake", allImages[645]);
		namesToImages.put("object.weapons.great dagger", allImages[646]);
		namesToImages.put("object.weapons.worm tooth", allImages[647]);
		namesToImages.put("object.weapons.knife", allImages[648]);
		namesToImages.put("object.weapons.stiletto", allImages[649]);
		namesToImages.put("object.weapons.scalpel", allImages[650]);
		namesToImages.put("object.weapons.crysknife", allImages[651]);
		namesToImages.put("object.weapons.axe", allImages[652]);
		namesToImages.put("object.weapons.double-headed axe.battle-axe",
				allImages[653]);
		namesToImages.put("object.weapons.broad pick.dwarvish mattock",
				allImages[654]);
		namesToImages.put(
				"object.weapons.crude short sword.orcish short sword",
				allImages[655]);
		namesToImages.put("object.weapons.short sword", allImages[656]);
		namesToImages.put("object.weapons.silver short sword", allImages[657]);
		namesToImages.put(
				"object.weapons.broad short sword.dwarvish short sword",
				allImages[658]);
		namesToImages.put("object.weapons.runed short sword.elven short sword",
				allImages[659]);
		namesToImages
				.put("object.weapons.black runed short sword.dark elven short sword",
						allImages[660]);
		namesToImages.put("object.weapons.broadsword", allImages[661]);
		namesToImages.put("object.weapons.runed broadsword.runesword",
				allImages[662]);
		namesToImages.put("object.weapons.runed broadsword.elven broadsword",
				allImages[663]);
		namesToImages.put("object.weapons.long sword", allImages[664]);
		namesToImages.put("object.weapons.silver long sword", allImages[665]);
		namesToImages
				.put("object.weapons.samurai sword.katana", allImages[666]);
		namesToImages.put("object.weapons.two-handed sword", allImages[667]);
		namesToImages.put("object.weapons.long samurai sword.tsurugi",
				allImages[668]);
		namesToImages.put("object.weapons.curved sword.scimitar",
				allImages[669]);
		namesToImages.put("object.weapons.rapier", allImages[670]);
		namesToImages.put("object.weapons.silver saber", allImages[671]);
		namesToImages.put("object.weapons.club", allImages[672]);
		namesToImages.put("object.weapons.thonged club.aklys", allImages[673]);
		namesToImages.put("object.weapons.baseball bat", allImages[674]);
		namesToImages.put("object.weapons.fly swatter", allImages[675]);
		namesToImages.put("object.weapons.silver mace", allImages[676]);
		namesToImages.put("object.weapons.mace", allImages[677]);
		namesToImages.put("object.weapons.morning star", allImages[678]);
		namesToImages.put("object.weapons.flail", allImages[679]);
		namesToImages.put("object.weapons.war hammer", allImages[680]);
		namesToImages.put("object.weapons.heavy hammer", allImages[681]);
		namesToImages.put("object.weapons.staff.quarterstaff", allImages[682]);
		namesToImages.put("object.weapons.vulgar polearm.partisan",
				allImages[683]);
		namesToImages.put("object.weapons.single-edged polearm.glaive",
				allImages[684]);
		namesToImages.put("object.weapons.forked polearm.spetum",
				allImages[685]);
		namesToImages.put("object.weapons.hilted polearm.ranseur",
				allImages[686]);
		namesToImages.put("object.weapons.long poleaxe.bardiche",
				allImages[687]);
		namesToImages.put("object.weapons.pole cleaver.voulge", allImages[688]);
		namesToImages.put("object.weapons.angled poleaxe.halberd",
				allImages[689]);
		namesToImages
				.put("object.weapons.pole sickle.fauchard", allImages[690]);
		namesToImages.put("object.weapons.pruning hook.guisarme",
				allImages[691]);
		namesToImages.put("object.weapons.hooked polearm.bill-guisarme",
				allImages[692]);
		namesToImages.put("object.weapons.pronged polearm.lucern hammer",
				allImages[693]);
		namesToImages.put("object.weapons.beaked polearm.bec de corbin",
				allImages[694]);
		namesToImages.put("object.weapons.crude spear.orcish spear",
				allImages[695]);
		namesToImages.put("object.weapons.spear", allImages[696]);
		namesToImages.put("object.weapons.silver spear", allImages[697]);
		namesToImages.put("object.weapons.runed spear.elven spear",
				allImages[698]);
		namesToImages.put("object.weapons.stout spear.dwarvish spear",
				allImages[699]);
		namesToImages.put("object.weapons.throwing spear.javelin",
				allImages[700]);
		namesToImages.put("object.weapons.trident", allImages[701]);
		namesToImages.put("object.weapons.lance", allImages[702]);
		namesToImages
				.put("object.weapons.crude bow.orcish bow", allImages[703]);
		namesToImages.put("object.weapons.bow", allImages[704]);
		namesToImages.put("object.weapons.runed bow.elven bow", allImages[705]);
		namesToImages.put("object.weapons.black runed bow.dark elven bow",
				allImages[706]);
		namesToImages.put("object.weapons.long bow.yumi", allImages[707]);
		namesToImages.put("object.weapons.crude arrow.orcish arrow",
				allImages[708]);
		namesToImages.put("object.weapons.arrow", allImages[709]);
		namesToImages.put("object.weapons.silver arrow", allImages[710]);
		namesToImages.put("object.weapons.runed arrow.elven arrow",
				allImages[711]);
		namesToImages.put("object.weapons.black runed arrow.dark elven arrow",
				allImages[712]);
		namesToImages.put("object.weapons.bamboo arrow.ya", allImages[713]);
		namesToImages.put("object.weapons.sling", allImages[714]);
		namesToImages.put("object.weapons.pistol", allImages[715]);
		namesToImages.put("object.weapons.submachine gun", allImages[716]);
		namesToImages.put("object.weapons.heavy machine gun", allImages[717]);
		namesToImages.put("object.weapons.rifle", allImages[718]);
		namesToImages.put("object.weapons.assault rifle", allImages[719]);
		namesToImages.put("object.weapons.sniper rifle", allImages[720]);
		namesToImages.put("object.weapons.shotgun", allImages[721]);
		namesToImages.put("object.weapons.auto shotgun", allImages[722]);
		namesToImages.put("object.weapons.rocket launcher", allImages[723]);
		namesToImages.put("object.weapons.grenade launcher", allImages[724]);
		namesToImages.put("object.weapons.bullet", allImages[725]);
		namesToImages.put("object.weapons.silver bullet", allImages[726]);
		namesToImages.put("object.weapons.shotgun shell", allImages[727]);
		namesToImages.put("object.weapons.rocket", allImages[728]);
		namesToImages.put("object.weapons.frag grenade", allImages[729]);
		namesToImages.put("object.weapons.gas grenade", allImages[730]);
		namesToImages.put("object.weapons.red stick.stick of dynamite",
				allImages[731]);
		namesToImages.put("object.weapons.crossbow", allImages[732]);
		namesToImages.put("object.weapons.crossbow bolt", allImages[733]);
		namesToImages.put("object.weapons.dart", allImages[734]);
		namesToImages.put("object.weapons.throwing star.shuriken",
				allImages[735]);
		namesToImages.put("object.weapons.boomerang", allImages[736]);
		namesToImages.put("object.weapons.bullwhip", allImages[737]);
		namesToImages.put("object.weapons.rubber hose", allImages[738]);
		namesToImages.put("object.armor.hawaiian shirt", allImages[739]);
		namesToImages.put("object.armor.t-shirt", allImages[740]);
		namesToImages.put("object.armor.plate mail", allImages[741]);
		namesToImages.put("object.armor.crystal plate mail", allImages[742]);
		namesToImages.put("object.armor.bronze plate mail", allImages[743]);
		namesToImages.put("object.armor.splint mail", allImages[744]);
		namesToImages.put("object.armor.banded mail", allImages[745]);
		namesToImages.put("object.armor.dwarvish mithril-coat", allImages[746]);
		namesToImages.put("object.armor.dark elven mithril-coat",
				allImages[747]);
		namesToImages.put("object.armor.elven mithril-coat", allImages[748]);
		namesToImages.put("object.armor.chain mail", allImages[749]);
		namesToImages.put("object.armor.crude chain mail.orcish chain mail",
				allImages[750]);
		namesToImages.put("object.armor.scale mail", allImages[751]);
		namesToImages.put("object.armor.studded leather armor", allImages[752]);
		namesToImages.put("object.armor.ring mail", allImages[753]);
		namesToImages.put("object.armor.crude ring mail.orcish ring mail",
				allImages[754]);
		namesToImages.put("object.armor.leather armor", allImages[755]);
		namesToImages.put("object.armor.leather jacket", allImages[756]);
		namesToImages.put("object.armor.red robe.robe", allImages[757]);
		namesToImages.put("object.armor.blue robe.robe of protection",
				allImages[758]);
		namesToImages.put("object.armor.orange robe.robe of power",
				allImages[759]);
		namesToImages.put("object.armor.green robe.robe of weakness",
				allImages[760]);
		namesToImages
				.put("object.armor.gray dragon scale mail", allImages[761]);
		namesToImages.put("object.armor.silver dragon scale mail",
				allImages[762]);
		namesToImages.put("object.armor.shimmering dragon scale mail",
				allImages[763]);
		namesToImages
				.put("object.armor.deep dragon scale mail", allImages[764]);
		namesToImages.put("object.armor.red dragon scale mail", allImages[765]);
		namesToImages.put("object.armor.white dragon scale mail",
				allImages[766]);
		namesToImages.put("object.armor.orange dragon scale mail",
				allImages[767]);
		namesToImages.put("object.armor.black dragon scale mail",
				allImages[768]);
		namesToImages
				.put("object.armor.blue dragon scale mail", allImages[769]);
		namesToImages.put("object.armor.green dragon scale mail",
				allImages[770]);
		namesToImages.put("object.armor.yellow dragon scale mail",
				allImages[771]);
		namesToImages.put("object.armor.gray dragon scales", allImages[772]);
		namesToImages.put("object.armor.silver dragon scales", allImages[773]);
		namesToImages.put("object.armor.shimmering dragon scales",
				allImages[774]);
		namesToImages.put("object.armor.deep dragon scales", allImages[775]);
		namesToImages.put("object.armor.red dragon scales", allImages[776]);
		namesToImages.put("object.armor.white dragon scales", allImages[777]);
		namesToImages.put("object.armor.orange dragon scales", allImages[778]);
		namesToImages.put("object.armor.black dragon scales", allImages[779]);
		namesToImages.put("object.armor.blue dragon scales", allImages[780]);
		namesToImages.put("object.armor.green dragon scales", allImages[781]);
		namesToImages.put("object.armor.yellow dragon scales", allImages[782]);
		namesToImages.put("object.armor.mummy wrapping", allImages[783]);
		namesToImages.put("object.armor.coarse mantelet.orcish cloak",
				allImages[784]);
		namesToImages.put("object.armor.hooded cloak.dwarvish cloak",
				allImages[785]);
		namesToImages.put("object.armor.slippery cloak.oilskin cloak",
				allImages[786]);
		namesToImages
				.put("object.armor.faded pall.elven cloak", allImages[787]);
		namesToImages.put("object.armor.white coat.lab coat", allImages[788]);
		namesToImages.put("object.armor.leather cloak", allImages[789]);
		namesToImages.put("object.armor.tattered cape.cloak of protection",
				allImages[790]);
		namesToImages.put("object.armor.dirty rag.poisonous cloak",
				allImages[791]);
		namesToImages.put("object.armor.opera cloak.cloak of invisibility",
				allImages[792]);
		namesToImages.put(
				"object.armor.ornamental cope.cloak of magic resistance",
				allImages[793]);
		namesToImages.put("object.armor.piece of cloth.cloak of displacement",
				allImages[794]);
		namesToImages.put("object.armor.leather hat.elven leather helm",
				allImages[795]);
		namesToImages.put("object.armor.iron skull cap.orcish helm",
				allImages[796]);
		namesToImages.put("object.armor.hard hat.dwarvish iron helm",
				allImages[797]);
		namesToImages.put("object.armor.fedora", allImages[798]);
		namesToImages
				.put("object.armor.conical hat.cornuthaum", allImages[799]);
		namesToImages.put("object.armor.conical hat.dunce cap", allImages[800]);
		namesToImages.put("object.armor.dented pot", allImages[801]);
		namesToImages.put("object.armor.plumed helmet.helmet", allImages[802]);
		namesToImages.put("object.armor.etched helmet.helm of brilliance",
				allImages[803]);
		namesToImages.put(
				"object.armor.crested helmet.helm of opposite alignment",
				allImages[804]);
		namesToImages.put("object.armor.visored helmet.helm of telepathy",
				allImages[805]);
		namesToImages.put("object.armor.old gloves.leather gloves",
				allImages[806]);
		namesToImages.put("object.armor.padded gloves.gauntlets of fumbling",
				allImages[807]);
		namesToImages.put("object.armor.riding gloves.gauntlets of power",
				allImages[808]);
		namesToImages.put("object.armor.black gloves.gauntlets of swimming",
				allImages[809]);
		namesToImages.put("object.armor.fencing gloves.gauntlets of dexterity",
				allImages[810]);
		namesToImages.put("object.armor.small shield", allImages[811]);
		namesToImages.put("object.armor.blue and green shield.elven shield",
				allImages[812]);
		namesToImages.put("object.armor.white-handed shield.uruk-hai shield",
				allImages[813]);
		namesToImages.put("object.armor.red-eyed shield.orcish shield",
				allImages[814]);
		namesToImages.put("object.armor.large shield", allImages[815]);
		namesToImages.put(
				"object.armor.large round shield.dwarvish roundshield",
				allImages[816]);
		namesToImages.put(
				"object.armor.polished silver shield.shield of reflection",
				allImages[817]);
		namesToImages.put("object.armor.walking shoes.low boots",
				allImages[818]);
		namesToImages.put("object.armor.hard shoes.iron shoes", allImages[819]);
		namesToImages.put("object.armor.jackboots.high boots", allImages[820]);
		namesToImages.put("object.armor.combat boots.speed boots",
				allImages[821]);
		namesToImages.put("object.armor.jungle boots.water walking boots",
				allImages[822]);
		namesToImages.put("object.armor.hiking boots.jumping boots",
				allImages[823]);
		namesToImages.put("object.armor.mud boots.elven boots", allImages[824]);
		namesToImages.put("object.armor.steel boots.kicking boots",
				allImages[825]);
		namesToImages.put("object.armor.riding boots.fumble boots",
				allImages[826]);
		namesToImages.put("object.armor.snow boots.levitation boots",
				allImages[827]);
		namesToImages.put("object.rings.wooden.adornment", allImages[828]);
		namesToImages.put("object.rings.topaz.hunger", allImages[829]);
		namesToImages.put("object.rings.ridged.mood", allImages[830]);
		namesToImages.put("object.rings.black onyx.protection", allImages[831]);
		namesToImages.put("object.rings.shiny.protection from shape changers",
				allImages[832]);
		namesToImages.put("object.rings.wedding.sleeping", allImages[833]);
		namesToImages.put("object.rings.jade.stealth", allImages[834]);
		namesToImages
				.put("object.rings.bronze.sustain ability", allImages[835]);
		namesToImages.put("object.rings.diamond.warning", allImages[836]);
		namesToImages.put("object.rings.sapphire.aggravate monster",
				allImages[837]);
		namesToImages.put("object.rings.brass.cold resistance", allImages[838]);
		namesToImages
				.put("object.rings.opal.gain constitution", allImages[839]);
		namesToImages.put("object.rings.obsidian.gain dexterity",
				allImages[840]);
		namesToImages.put("object.rings.plain.gain intelligence",
				allImages[841]);
		namesToImages.put("object.rings.granite.gain strength", allImages[842]);
		namesToImages.put("object.rings.glass.gain wisdom", allImages[843]);
		namesToImages
				.put("object.rings.clay.increase accuracy", allImages[844]);
		namesToImages.put("object.rings.coral.increase damage", allImages[845]);
		namesToImages.put("object.rings.steel.slow digestion", allImages[846]);
		namesToImages.put("object.rings.wire.invisibility", allImages[847]);
		namesToImages.put("object.rings.pearl.poison resistance",
				allImages[848]);
		namesToImages.put("object.rings.engagement.see invisible",
				allImages[849]);
		namesToImages.put("object.rings.copper.shock resistance",
				allImages[850]);
		namesToImages.put("object.rings.iron.fire resistance", allImages[851]);
		namesToImages.put("object.rings.twisted.free action", allImages[852]);
		namesToImages.put("object.rings.agate.levitation", allImages[853]);
		namesToImages
				.put("object.rings.moonstone.regeneration", allImages[854]);
		namesToImages.put("object.rings.tiger eye.searching", allImages[855]);
		namesToImages.put("object.rings.silver.teleportation", allImages[856]);
		namesToImages.put("object.rings.ruby.conflict", allImages[857]);
		namesToImages.put("object.rings.ivory.polymorph", allImages[858]);
		namesToImages.put("object.rings.emerald.polymorph control",
				allImages[859]);
		namesToImages.put("object.rings.gold.teleport control", allImages[860]);
		namesToImages.put("object.amulets.square.amulet of change",
				allImages[861]);
		namesToImages.put("object.amulets.warped.amulet of drain resistance",
				allImages[862]);
		namesToImages.put("object.amulets.circular.amulet of esp",
				allImages[863]);
		namesToImages.put("object.amulets.convex.amulet of flying",
				allImages[864]);
		namesToImages.put("object.amulets.spherical.amulet of life saving",
				allImages[865]);
		namesToImages.put(
				"object.amulets.octagonal.amulet of magical breathing",
				allImages[866]);
		namesToImages.put("object.amulets.hexagonal.amulet of reflection",
				allImages[867]);
		namesToImages.put("object.amulets.triangular.amulet of restful sleep",
				allImages[868]);
		namesToImages.put("object.amulets.oval.amulet of strangulation",
				allImages[869]);
		namesToImages.put("object.amulets.concave.amulet of unchanging",
				allImages[870]);
		namesToImages.put("object.amulets.pyramidal.amulet versus poison",
				allImages[871]);
		namesToImages.put("object.amulets.lunate.amulet versus stone",
				allImages[872]);
		namesToImages
				.put("object.amulets.amulet of yendor.cheap plastic imitation of the amulet of yendor",
						allImages[873]);
		namesToImages.put("object.amulets.amulet of yendor", allImages[874]);
		namesToImages.put("object.tools.large box", allImages[875]);
		namesToImages.put("object.tools.chest", allImages[876]);
		namesToImages.put("object.tools.ice box", allImages[877]);
		namesToImages.put("object.tools.bag.sack", allImages[878]);
		namesToImages.put("object.tools.bag.oilskin sack", allImages[879]);
		namesToImages.put("object.tools.bag.bag of holding", allImages[880]);
		namesToImages.put("object.tools.bag.bag of tricks", allImages[881]);
		namesToImages.put("object.tools.key.skeleton key", allImages[882]);
		namesToImages.put("object.tools.lock pick", allImages[883]);
		namesToImages.put("object.tools.credit card", allImages[884]);
		namesToImages.put("object.tools.candle.tallow candle", allImages[885]);
		namesToImages.put("object.tools.candle.wax candle", allImages[886]);
		namesToImages.put("object.tools.candle.magic candle", allImages[887]);
		namesToImages.put("object.tools.lamp.oil lamp", allImages[888]);
		namesToImages.put("object.tools.brass lantern", allImages[889]);
		namesToImages.put("object.tools.lamp.magic lamp", allImages[890]);
		namesToImages.put("object.tools.whistle.tin whistle", allImages[891]);
		namesToImages.put("object.tools.whistle.magic whistle", allImages[892]);
		namesToImages.put("object.tools.flute.wooden flute", allImages[893]);
		namesToImages.put("object.tools.flute.magic flute", allImages[894]);
		namesToImages.put("object.tools.horn.tooled horn", allImages[895]);
		namesToImages.put("object.tools.horn.frost horn", allImages[896]);
		namesToImages.put("object.tools.horn.fire horn", allImages[897]);
		namesToImages.put("object.tools.horn.horn of plenty", allImages[898]);
		namesToImages.put("object.tools.harp.wooden harp", allImages[899]);
		namesToImages.put("object.tools.harp.magic harp", allImages[900]);
		namesToImages.put("object.tools.bell", allImages[901]);
		namesToImages.put("object.tools.bugle", allImages[902]);
		namesToImages.put("object.tools.drum.leather drum", allImages[903]);
		namesToImages.put("object.tools.drum.drum of earthquake",
				allImages[904]);
		namesToImages.put("object.tools.land mine", allImages[905]);
		namesToImages.put("object.tools.beartrap", allImages[906]);
		namesToImages.put("object.tools.spoon", allImages[907]);
		namesToImages.put("object.tools.pick-axe", allImages[908]);
		namesToImages.put("object.tools.fishing pole", allImages[909]);
		namesToImages.put("object.tools.iron hook.grappling hook",
				allImages[910]);
		namesToImages.put("object.tools.unicorn horn", allImages[911]);
		namesToImages.put("object.tools.torch", allImages[912]);
		namesToImages.put("object.tools.lightsaber.green lightsaber",
				allImages[913]);
		namesToImages.put("object.tools.lightsaber.blue lightsaber",
				allImages[914]);
		namesToImages.put("object.tools.lightsaber.red lightsaber",
				allImages[915]);
		namesToImages.put(
				"object.tools.double lightsaber.red double lightsaber",
				allImages[916]);
		namesToImages.put("object.tools.expensive camera", allImages[917]);
		namesToImages.put("object.tools.looking glass.mirror", allImages[918]);
		namesToImages
				.put("object.tools.glass orb.crystal ball", allImages[919]);
		namesToImages.put("object.tools.lenses", allImages[920]);
		namesToImages.put("object.tools.blindfold", allImages[921]);
		namesToImages.put("object.tools.towel", allImages[922]);
		namesToImages.put("object.tools.saddle", allImages[923]);
		namesToImages.put("object.tools.leash", allImages[924]);
		namesToImages.put("object.tools.stethoscope", allImages[925]);
		namesToImages.put("object.tools.tinning kit", allImages[926]);
		namesToImages.put("object.tools.leather bag.medical kit",
				allImages[927]);
		namesToImages.put("object.tools.tin opener", allImages[928]);
		namesToImages.put("object.tools.can of grease", allImages[929]);
		namesToImages.put("object.tools.figurine", allImages[930]);
		namesToImages.put("object.tools.magic marker", allImages[931]);
		namesToImages.put("object.tools.bandage", allImages[932]);
		namesToImages.put("object.tools.phial", allImages[933]);
		namesToImages.put("object.tools.candelabrum.candelabrum of invocation",
				allImages[934]);
		namesToImages.put("object.tools.silver bell.bell of opening",
				allImages[935]);
		namesToImages.put("object.food.tripe ration", allImages[936]);
		namesToImages.put("object.food.corpse", allImages[937]);
		namesToImages.put("object.food.egg", allImages[938]);
		namesToImages.put("object.food.meatball", allImages[939]);
		namesToImages.put("object.food.meat stick", allImages[940]);
		namesToImages.put("object.food.huge chunk of meat", allImages[941]);
		namesToImages.put("object.food.meat ring", allImages[942]);
		namesToImages.put("object.food.eyeball", allImages[943]);
		namesToImages.put("object.food.severed hand", allImages[944]);
		namesToImages.put("object.food.kelp frond", allImages[945]);
		namesToImages.put("object.food.eucalyptus leaf", allImages[946]);
		namesToImages.put("object.food.clove of garlic", allImages[947]);
		namesToImages.put("object.food.sprig of wolfsbane", allImages[948]);
		namesToImages.put("object.food.apple", allImages[949]);
		namesToImages.put("object.food.carrot", allImages[950]);
		namesToImages.put("object.food.pear", allImages[951]);
		namesToImages.put("object.food.asian pear", allImages[952]);
		namesToImages.put("object.food.banana", allImages[953]);
		namesToImages.put("object.food.orange", allImages[954]);
		namesToImages.put("object.food.mushroom", allImages[955]);
		namesToImages.put("object.food.melon", allImages[956]);
		namesToImages.put("object.food.slime mold", allImages[957]);
		namesToImages.put("object.food.lump of royal jelly", allImages[958]);
		namesToImages.put("object.food.cream pie", allImages[959]);
		namesToImages.put("object.food.sandwich", allImages[960]);
		namesToImages.put("object.food.candy bar", allImages[961]);
		namesToImages.put("object.food.fortune cookie", allImages[962]);
		namesToImages.put("object.food.pancake", allImages[963]);
		namesToImages.put("object.food.tortilla", allImages[964]);
		namesToImages.put("object.food.cheese", allImages[965]);
		namesToImages.put("object.food.pill", allImages[966]);
		namesToImages.put("object.food.holy wafer", allImages[967]);
		namesToImages.put("object.food.lembas wafer", allImages[968]);
		namesToImages.put("object.food.cram ration", allImages[969]);
		namesToImages.put("object.food.food ration", allImages[970]);
		namesToImages.put("object.food.k-ration", allImages[971]);
		namesToImages.put("object.food.c-ration", allImages[972]);
		namesToImages.put("object.food.tin", allImages[973]);
		namesToImages.put("object.potions.brown.booze", allImages[974]);
		namesToImages.put("object.potions.dark.fruit juice", allImages[975]);
		namesToImages.put("object.potions.magenta.see invisible",
				allImages[976]);
		namesToImages.put("object.potions.fizzy.sickness", allImages[977]);
		namesToImages.put("object.potions.effervescent.sleeping",
				allImages[978]);
		namesToImages.put("object.potions.luminescent.clairvoyance",
				allImages[979]);
		namesToImages.put("object.potions.orange.confusion", allImages[980]);
		namesToImages.put("object.potions.sky blue.hallucination",
				allImages[981]);
		namesToImages.put("object.potions.purple-red.healing", allImages[982]);
		namesToImages.put("object.potions.puce.extra healing", allImages[983]);
		namesToImages
				.put("object.potions.pink.restore ability", allImages[984]);
		namesToImages.put("object.potions.yellow.blindness", allImages[985]);
		namesToImages.put("object.potions.muddy.esp", allImages[986]);
		namesToImages.put("object.potions.cloudy.gain energy", allImages[987]);
		namesToImages.put("object.potions.brilliant blue.invisibility",
				allImages[988]);
		namesToImages.put("object.potions.bubbly.monster detection",
				allImages[989]);
		namesToImages.put("object.potions.smoky.object detection",
				allImages[990]);
		namesToImages
				.put("object.potions.swirly.enlightenment", allImages[991]);
		namesToImages.put("object.potions.black.full healing", allImages[992]);
		namesToImages.put("object.potions.cyan.levitation", allImages[993]);
		namesToImages.put("object.potions.golden.polymorph", allImages[994]);
		namesToImages.put("object.potions.dark.speed", allImages[995]);
		namesToImages.put("object.potions.white.acid", allImages[996]);
		namesToImages.put("object.potions.murky.oil", allImages[997]);
		namesToImages.put("object.potions.ruby.gain ability", allImages[998]);
		namesToImages.put("object.potions.milky.gain level", allImages[999]);
		namesToImages
				.put("object.potions.icy.invulnerability", allImages[1000]);
		namesToImages.put("object.potions.emerald.paralysis", allImages[1001]);
		namesToImages.put("object.potions.clear.water", allImages[1002]);
		namesToImages.put("object.potions.blood-red.blood", allImages[1003]);
		namesToImages.put("object.potions.blood-red.vampire blood",
				allImages[1004]);
		namesToImages.put("object.potions.sparkling.amnesia", allImages[1005]);
		namesToImages.put("object.scrolls.lep gex ven zea.create monster",
				allImages[1006]);
		namesToImages.put("object.scrolls.prirutsenie.taming", allImages[1007]);
		namesToImages.put("object.scrolls.verr yed horre.light",
				allImages[1008]);
		namesToImages.put("object.scrolls.yum yum.food detection",
				allImages[1009]);
		namesToImages.put("object.scrolls.tharr.gold detection",
				allImages[1010]);
		namesToImages
				.put("object.scrolls.kernod wel.identify", allImages[1011]);
		namesToImages.put("object.scrolls.elam ebow.magic mapping",
				allImages[1012]);
		namesToImages.put("object.scrolls.nr 9.confuse monster",
				allImages[1013]);
		namesToImages.put("object.scrolls.xixaxa xoxaxa xuxaxa.scare monster",
				allImages[1014]);
		namesToImages.put("object.scrolls.daiyen fooels.enchant weapon",
				allImages[1015]);
		namesToImages.put("object.scrolls.zelgo mer.enchant armor",
				allImages[1016]);
		namesToImages.put("object.scrolls.pratyavayah.remove curse",
				allImages[1017]);
		namesToImages.put("object.scrolls.venzar borgavve.teleportation",
				allImages[1018]);
		namesToImages
				.put("object.scrolls.andova begarin.fire", allImages[1019]);
		namesToImages.put("object.scrolls.kirje.earth", allImages[1020]);
		namesToImages.put("object.scrolls.juyed awk yacc.destroy armor",
				allImages[1021]);
		namesToImages.put("object.scrolls.duam xnaht.amnesia", allImages[1022]);
		namesToImages.put("object.scrolls.hackem muche.charging",
				allImages[1023]);
		namesToImages
				.put("object.scrolls.elbib yloh.genocide", allImages[1024]);
		namesToImages.put("object.scrolls.ve forbryderne.punishment",
				allImages[1025]);
		namesToImages.put("object.scrolls.velox neb.stinking cloud",
				allImages[1026]);
		namesToImages.put("object.scrolls.foobie bletch", allImages[1027]);
		namesToImages.put("object.scrolls.temov", allImages[1028]);
		namesToImages.put("object.scrolls.garven deh", allImages[1029]);
		namesToImages.put("object.scrolls.read me", allImages[1030]);
		namesToImages.put("object.scrolls.stamped.mail", allImages[1031]);
		namesToImages.put("object.scrolls.unlabeled.blank paper",
				allImages[1032]);
		namesToImages.put("object.spell books.red.force bolt", allImages[1033]);
		namesToImages.put("object.spell books.turquoise.create monster",
				allImages[1034]);
		namesToImages.put("object.spell books.velvet.drain life",
				allImages[1035]);
		namesToImages.put("object.spell books.dark.command undead",
				allImages[1036]);
		namesToImages.put("object.spell books.black.summon undead",
				allImages[1037]);
		namesToImages.put("object.spell books.thick.stone to flesh",
				allImages[1038]);
		namesToImages.put("object.spell books.white.healing", allImages[1039]);
		namesToImages.put("object.spell books.yellow.cure blindness",
				allImages[1040]);
		namesToImages.put("object.spell books.indigo.cure sickness",
				allImages[1041]);
		namesToImages.put("object.spell books.plaid.extra healing",
				allImages[1042]);
		namesToImages.put("object.spell books.light brown.restore ability",
				allImages[1043]);
		namesToImages.put("object.spell books.glittering.create familiar",
				allImages[1044]);
		namesToImages.put("object.spell books.cloth.light", allImages[1045]);
		namesToImages.put("object.spell books.leather.detect monsters",
				allImages[1046]);
		namesToImages.put("object.spell books.cyan.detect food",
				allImages[1047]);
		namesToImages.put("object.spell books.dark blue.clairvoyance",
				allImages[1048]);
		namesToImages.put("object.spell books.violet.detect unseen",
				allImages[1049]);
		namesToImages
				.put("object.spell books.bronze.identify", allImages[1050]);
		namesToImages.put("object.spell books.gray.detect treasure",
				allImages[1051]);
		namesToImages.put("object.spell books.dusty.magic mapping",
				allImages[1052]);
		namesToImages.put("object.spell books.orange.confuse monster",
				allImages[1053]);
		namesToImages.put("object.spell books.light green.slow monster",
				allImages[1054]);
		namesToImages.put("object.spell books.light blue.cause fear",
				allImages[1055]);
		namesToImages.put("object.spell books.magenta.charm monster",
				allImages[1056]);
		namesToImages.put("object.spell books.dull.enchant weapon",
				allImages[1057]);
		namesToImages.put("object.spell books.thin.enchant armor",
				allImages[1058]);
		namesToImages
				.put("object.spell books.wide.protection", allImages[1059]);
		namesToImages.put("object.spell books.big.resist poison",
				allImages[1060]);
		namesToImages.put("object.spell books.fuzzy.resist sleep",
				allImages[1061]);
		namesToImages.put("object.spell books.deep.endure cold",
				allImages[1062]);
		namesToImages.put("object.spell books.spotted.endure heat",
				allImages[1063]);
		namesToImages.put("object.spell books.long.insulate", allImages[1064]);
		namesToImages.put("object.spell books.wrinkled.remove curse",
				allImages[1065]);
		namesToImages.put("object.spell books.copper.turn undead",
				allImages[1066]);
		namesToImages.put("object.spell books.torn.jumping", allImages[1067]);
		namesToImages.put("object.spell books.purple.haste self",
				allImages[1068]);
		namesToImages
				.put("object.spell books.faded.enlighten", allImages[1069]);
		namesToImages.put("object.spell books.dark brown.invisibility",
				allImages[1070]);
		namesToImages.put("object.spell books.tan.levitation", allImages[1071]);
		namesToImages.put("object.spell books.gold.teleport away",
				allImages[1072]);
		namesToImages.put("object.spell books.ochre.passwall", allImages[1073]);
		namesToImages.put("object.spell books.silver.polymorph",
				allImages[1074]);
		namesToImages.put("object.spell books.pink.knock", allImages[1075]);
		namesToImages.put("object.spell books.canvas.flame sphere",
				allImages[1076]);
		namesToImages.put("object.spell books.hardcover.freeze sphere",
				allImages[1077]);
		namesToImages.put("object.spell books.dark green.wizard lock",
				allImages[1078]);
		namesToImages.put("object.spell books.parchment.dig", allImages[1079]);
		namesToImages.put("object.spell books.shining.cancellation",
				allImages[1080]);
		namesToImages.put("object.spell books.vellum.magic missile",
				allImages[1081]);
		namesToImages
				.put("object.spell books.ragged.fireball", allImages[1082]);
		namesToImages.put("object.spell books.dog eared.cone of cold",
				allImages[1083]);
		namesToImages.put("object.spell books.mottled.sleep", allImages[1084]);
		namesToImages.put("object.spell books.stained.finger of death",
				allImages[1085]);
		namesToImages.put("object.spell books.rainbow.lightning",
				allImages[1086]);
		namesToImages.put("object.spell books.tattered.poison blast",
				allImages[1087]);
		namesToImages.put("object.spell books.colorful.acid stream",
				allImages[1088]);
		namesToImages.put("object.spell books.tartan", allImages[1089]);
		namesToImages.put("object.spell books.stylish", allImages[1090]);
		namesToImages.put("object.spell books.psychedelic", allImages[1091]);
		namesToImages.put("object.spell books.spiral-bound", allImages[1092]);
		namesToImages.put("object.spell books.left-handed", allImages[1093]);
		namesToImages.put("object.spell books.stapled", allImages[1094]);
		namesToImages.put("object.spell books.plain.blank paper",
				allImages[1095]);
		namesToImages.put("object.spell books.papyrus.book of the dead",
				allImages[1096]);
		namesToImages.put("object.wands.glass.light", allImages[1097]);
		namesToImages.put("object.wands.oak.nothing", allImages[1098]);
		namesToImages
				.put("object.wands.crystal.enlightenment", allImages[1099]);
		namesToImages.put("object.wands.bamboo.healing", allImages[1100]);
		namesToImages.put("object.wands.aluminum.locking", allImages[1101]);
		namesToImages
				.put("object.wands.marble.make invisible", allImages[1102]);
		namesToImages.put("object.wands.zinc.opening", allImages[1103]);
		namesToImages.put("object.wands.uranium.probing", allImages[1104]);
		namesToImages.put("object.wands.balsa.secret door detection",
				allImages[1105]);
		namesToImages.put("object.wands.tin.slow monster", allImages[1106]);
		namesToImages.put("object.wands.brass.speed monster", allImages[1107]);
		namesToImages.put("object.wands.ebony.striking", allImages[1108]);
		namesToImages
				.put("object.wands.copper.undead turning", allImages[1109]);
		namesToImages.put("object.wands.ceramic.draining", allImages[1110]);
		namesToImages
				.put("object.wands.platinum.cancellation", allImages[1111]);
		namesToImages.put("object.wands.maple.create monster", allImages[1112]);
		namesToImages.put("object.wands.rusty.fear", allImages[1113]);
		namesToImages.put("object.wands.silver.polymorph", allImages[1114]);
		namesToImages
				.put("object.wands.iridium.teleportation", allImages[1115]);
		namesToImages.put("object.wands.black.create horde", allImages[1116]);
		namesToImages.put("object.wands.bronze.extra healing", allImages[1117]);
		namesToImages.put("object.wands.pine.wishing", allImages[1118]);
		namesToImages.put("object.wands.iron.digging", allImages[1119]);
		namesToImages.put("object.wands.steel.magic missile", allImages[1120]);
		namesToImages.put("object.wands.hexagonal.fire", allImages[1121]);
		namesToImages.put("object.wands.short.cold", allImages[1122]);
		namesToImages.put("object.wands.runed.sleep", allImages[1123]);
		namesToImages.put("object.wands.long.death", allImages[1124]);
		namesToImages.put("object.wands.curved.lightning", allImages[1125]);
		namesToImages.put("object.wands.octagonal.fireball", allImages[1126]);
		namesToImages.put("object.wands.forked", allImages[1127]);
		namesToImages.put("object.wands.spiked", allImages[1128]);
		namesToImages.put("object.wands.jeweled", allImages[1129]);
		namesToImages.put("object.coins.gold piece", allImages[1130]);
		namesToImages.put("object.rocks.white.dilithium crystal",
				allImages[1131]);
		namesToImages.put("object.rocks.white.diamond", allImages[1132]);
		namesToImages.put("object.rocks.red.ruby", allImages[1133]);
		namesToImages.put("object.rocks.orange.jacinth", allImages[1134]);
		namesToImages.put("object.rocks.blue.sapphire", allImages[1135]);
		namesToImages.put("object.rocks.black.black opal", allImages[1136]);
		namesToImages.put("object.rocks.green.emerald", allImages[1137]);
		namesToImages.put("object.rocks.green.turquoise", allImages[1138]);
		namesToImages.put("object.rocks.yellow.citrine", allImages[1139]);
		namesToImages.put("object.rocks.green.aquamarine", allImages[1140]);
		namesToImages
				.put("object.rocks.yellowish brown.amber", allImages[1141]);
		namesToImages
				.put("object.rocks.yellowish brown.topaz", allImages[1142]);
		namesToImages.put("object.rocks.black.jet", allImages[1143]);
		namesToImages.put("object.rocks.white.opal", allImages[1144]);
		namesToImages.put("object.rocks.yellow.chrysoberyl", allImages[1145]);
		namesToImages.put("object.rocks.red.garnet", allImages[1146]);
		namesToImages.put("object.rocks.violet.amethyst", allImages[1147]);
		namesToImages.put("object.rocks.red.jasper", allImages[1148]);
		namesToImages.put("object.rocks.violet.fluorite", allImages[1149]);
		namesToImages.put("object.rocks.black.obsidian", allImages[1150]);
		namesToImages.put("object.rocks.orange.agate", allImages[1151]);
		namesToImages.put("object.rocks.green.jade", allImages[1152]);
		namesToImages.put("object.rocks.white.worthless piece of white glass",
				allImages[1153]);
		namesToImages.put("object.rocks.blue.worthless piece of blue glass",
				allImages[1154]);
		namesToImages.put("object.rocks.red.worthless piece of red glass",
				allImages[1155]);
		namesToImages
				.put("object.rocks.yellowish brown.worthless piece of yellowish brown glass",
						allImages[1156]);
		namesToImages.put(
				"object.rocks.orange.worthless piece of orange glass",
				allImages[1157]);
		namesToImages.put(
				"object.rocks.yellow.worthless piece of yellow glass",
				allImages[1158]);
		namesToImages.put("object.rocks.black.worthless piece of black glass",
				allImages[1159]);
		namesToImages.put("object.rocks.green.worthless piece of green glass",
				allImages[1160]);
		namesToImages.put(
				"object.rocks.violet.worthless piece of violet glass",
				allImages[1161]);
		namesToImages.put("object.rocks.gray.luckstone", allImages[1162]);
		namesToImages.put("object.rocks.gray.healthstone", allImages[1163]);
		namesToImages.put("object.rocks.gray.loadstone", allImages[1164]);
		namesToImages.put("object.rocks.gray.touchstone", allImages[1165]);
		namesToImages.put("object.rocks.gray.whetstone", allImages[1166]);
		namesToImages.put("object.rocks.gray.flint", allImages[1167]);
		namesToImages.put("object.rocks.rock", allImages[1168]);
		namesToImages.put("object.large stones.boulder", allImages[1169]);
		namesToImages.put("object.large stones.statue", allImages[1170]);
		namesToImages.put("object.iron balls.heavy iron ball", allImages[1171]);
		namesToImages.put("object.chains.iron chain", allImages[1172]);
		namesToImages.put("object.venoms.splash of venom.blinding venom",
				allImages[1173]);
		namesToImages.put("object.venoms.splash of venom.acid venom",
				allImages[1174]);
		namesToImages.put("cmap.dark part of a room", allImages[1175]);
		namesToImages.put("cmap.wall.vertical", allImages[1176]);
		namesToImages.put("cmap.wall.horizontal", allImages[1177]);
		namesToImages.put("cmap.wall.top left corner", allImages[1178]);
		namesToImages.put("cmap.wall.top right corner", allImages[1179]);
		namesToImages.put("cmap.wall.bottom left corner", allImages[1180]);
		namesToImages.put("cmap.wall.bottom right corner", allImages[1181]);
		namesToImages.put("cmap.wall.crosswall", allImages[1182]);
		namesToImages.put("cmap.wall.tee up", allImages[1183]);
		namesToImages.put("cmap.wall.tee down", allImages[1184]);
		namesToImages.put("cmap.wall.tee left", allImages[1185]);
		namesToImages.put("cmap.wall.tee right", allImages[1186]);
		namesToImages.put("cmap.door.doorway", allImages[1187]);
		namesToImages.put("cmap.door.vertical open door", allImages[1188]);
		namesToImages.put("cmap.door.horizontal open door", allImages[1189]);
		namesToImages.put("cmap.door.vertical closed door", allImages[1190]);
		namesToImages.put("cmap.door.horizontal closed door", allImages[1191]);
		namesToImages.put("cmap.iron bars", allImages[1192]);
		namesToImages.put("cmap.tree", allImages[1193]);
		namesToImages.put("cmap.floor of a room", allImages[1194]);
		namesToImages.put("cmap.corridor", allImages[1195]);
		namesToImages.put("cmap.lit corridor", allImages[1196]);
		namesToImages.put("cmap.staircase up", allImages[1197]);
		namesToImages.put("cmap.staircase down", allImages[1198]);
		namesToImages.put("cmap.ladder up", allImages[1199]);
		namesToImages.put("cmap.ladder down", allImages[1200]);
		namesToImages.put("cmap.altar", allImages[1201]);
		namesToImages.put("cmap.grave", allImages[1202]);
		namesToImages.put("cmap.opulent throne", allImages[1203]);
		namesToImages.put("cmap.sink", allImages[1204]);
		namesToImages.put("cmap.toilet", allImages[1205]);
		namesToImages.put("cmap.fountain", allImages[1206]);
		namesToImages.put("cmap.pool", allImages[1207]);
		namesToImages.put("cmap.ice", allImages[1208]);
		namesToImages.put("cmap.molten lava", allImages[1209]);
		namesToImages.put("cmap.lowered drawbridge.vertical", allImages[1210]);
		namesToImages
				.put("cmap.lowered drawbridge.horizontal", allImages[1211]);
		namesToImages.put("cmap.raised drawbridge.horizontal", allImages[1212]);
		namesToImages.put("cmap.raised drawbridge.vertical", allImages[1213]);
		namesToImages.put("cmap.air", allImages[1214]);
		namesToImages.put("cmap.cloud", allImages[1215]);
		namesToImages.put("cmap.water", allImages[1216]);
		namesToImages.put("cmap.trap.arrow trap", allImages[1217]);
		namesToImages.put("cmap.trap.dart trap", allImages[1218]);
		namesToImages.put("cmap.trap.falling rock trap", allImages[1219]);
		namesToImages.put("cmap.trap.squeaky board", allImages[1220]);
		namesToImages.put("cmap.trap.bear trap", allImages[1221]);
		namesToImages.put("cmap.trap.land mine", allImages[1222]);
		namesToImages.put("cmap.trap.rolling boulder trap", allImages[1223]);
		namesToImages.put("cmap.trap.sleeping gas trap", allImages[1224]);
		namesToImages.put("cmap.trap.rust trap", allImages[1225]);
		namesToImages.put("cmap.trap.fire trap", allImages[1226]);
		namesToImages.put("cmap.trap.pit", allImages[1227]);
		namesToImages.put("cmap.trap.spiked pit", allImages[1228]);
		namesToImages.put("cmap.trap.hole", allImages[1229]);
		namesToImages.put("cmap.trap.trap door", allImages[1230]);
		namesToImages.put("cmap.trap.teleportation trap", allImages[1231]);
		namesToImages.put("cmap.trap.level teleporter", allImages[1232]);
		namesToImages.put("cmap.trap.magic portal", allImages[1233]);
		namesToImages.put("cmap.trap.web", allImages[1234]);
		namesToImages.put("cmap.trap.statue trap", allImages[1235]);
		namesToImages.put("cmap.trap.magic trap", allImages[1236]);
		namesToImages.put("cmap.trap.anti-magic field", allImages[1237]);
		namesToImages.put("cmap.trap.polymorph trap", allImages[1238]);
		namesToImages.put("cmap.effect.vertical beam", allImages[1239]);
		namesToImages.put("cmap.effect.horizontal beam", allImages[1240]);
		namesToImages.put("cmap.effect.left slant beam", allImages[1241]);
		namesToImages.put("cmap.effect.right slant beam", allImages[1242]);
		namesToImages.put("cmap.effect.dig beam", allImages[1243]);
		namesToImages.put("cmap.effect.camera flash", allImages[1244]);
		namesToImages.put("cmap.effect.thrown boomerang.open left",
				allImages[1245]);
		namesToImages.put("cmap.effect.thrown boomerang.open right",
				allImages[1246]);
		namesToImages.put("cmap.effect.magic shield 1", allImages[1247]);
		namesToImages.put("cmap.effect.magic shield 2", allImages[1248]);
		namesToImages.put("cmap.effect.magic shield 3", allImages[1249]);
		namesToImages.put("cmap.effect.magic shield 4", allImages[1250]);
		namesToImages.put("cmap.swallow.top left", allImages[1251]);
		namesToImages.put("cmap.swallow.top center", allImages[1252]);
		namesToImages.put("cmap.swallow.top right", allImages[1253]);
		namesToImages.put("cmap.swallow.middle left", allImages[1254]);
		namesToImages.put("cmap.swallow.middle right", allImages[1255]);
		namesToImages.put("cmap.swallow.bottom left", allImages[1256]);
		namesToImages.put("cmap.swallow.bottom center", allImages[1257]);
		namesToImages.put("cmap.swallow.bottom right", allImages[1258]);
		namesToImages.put("explosion.dark.top left", allImages[1259]);
		namesToImages.put("explosion.dark.top center", allImages[1260]);
		namesToImages.put("explosion.dark.top right", allImages[1261]);
		namesToImages.put("explosion.dark.middle left", allImages[1262]);
		namesToImages.put("explosion.dark.middle center", allImages[1263]);
		namesToImages.put("explosion.dark.middle right", allImages[1264]);
		namesToImages.put("explosion.dark.bottom left", allImages[1265]);
		namesToImages.put("explosion.dark.bottom center", allImages[1266]);
		namesToImages.put("explosion.dark.bottom right", allImages[1267]);
		namesToImages.put("explosion.noxious.top left", allImages[1268]);
		namesToImages.put("explosion.noxious.top center", allImages[1269]);
		namesToImages.put("explosion.noxious.top right", allImages[1270]);
		namesToImages.put("explosion.noxious.middle left", allImages[1271]);
		namesToImages.put("explosion.noxious.middle center", allImages[1272]);
		namesToImages.put("explosion.noxious.middle right", allImages[1273]);
		namesToImages.put("explosion.noxious.bottom left", allImages[1274]);
		namesToImages.put("explosion.noxious.bottom center", allImages[1275]);
		namesToImages.put("explosion.noxious.bottom right", allImages[1276]);
		namesToImages.put("explosion.muddy.top left", allImages[1277]);
		namesToImages.put("explosion.muddy.top center", allImages[1278]);
		namesToImages.put("explosion.muddy.top right", allImages[1279]);
		namesToImages.put("explosion.muddy.middle left", allImages[1280]);
		namesToImages.put("explosion.muddy.middle center", allImages[1281]);
		namesToImages.put("explosion.muddy.middle right", allImages[1282]);
		namesToImages.put("explosion.muddy.bottom left", allImages[1283]);
		namesToImages.put("explosion.muddy.bottom center", allImages[1284]);
		namesToImages.put("explosion.muddy.bottom right", allImages[1285]);
		namesToImages.put("explosion.wet.top left", allImages[1286]);
		namesToImages.put("explosion.wet.top center", allImages[1287]);
		namesToImages.put("explosion.wet.top right", allImages[1288]);
		namesToImages.put("explosion.wet.middle left", allImages[1289]);
		namesToImages.put("explosion.wet.middle center", allImages[1290]);
		namesToImages.put("explosion.wet.middle right", allImages[1291]);
		namesToImages.put("explosion.wet.bottom left", allImages[1292]);
		namesToImages.put("explosion.wet.bottom center", allImages[1293]);
		namesToImages.put("explosion.wet.bottom right", allImages[1294]);
		namesToImages.put("explosion.magical.top left", allImages[1295]);
		namesToImages.put("explosion.magical.top center", allImages[1296]);
		namesToImages.put("explosion.magical.top right", allImages[1297]);
		namesToImages.put("explosion.magical.middle left", allImages[1298]);
		namesToImages.put("explosion.magical.middle center", allImages[1299]);
		namesToImages.put("explosion.magical.middle right", allImages[1300]);
		namesToImages.put("explosion.magical.bottom left", allImages[1301]);
		namesToImages.put("explosion.magical.bottom center", allImages[1302]);
		namesToImages.put("explosion.magical.bottom right", allImages[1303]);
		namesToImages.put("explosion.fiery.top left", allImages[1304]);
		namesToImages.put("explosion.fiery.top center", allImages[1305]);
		namesToImages.put("explosion.fiery.top right", allImages[1306]);
		namesToImages.put("explosion.fiery.middle left", allImages[1307]);
		namesToImages.put("explosion.fiery.middle center", allImages[1308]);
		namesToImages.put("explosion.fiery.middle right", allImages[1309]);
		namesToImages.put("explosion.fiery.bottom left", allImages[1310]);
		namesToImages.put("explosion.fiery.bottom center", allImages[1311]);
		namesToImages.put("explosion.fiery.bottom right", allImages[1312]);
		namesToImages.put("explosion.frosty.top left", allImages[1313]);
		namesToImages.put("explosion.frosty.top center", allImages[1314]);
		namesToImages.put("explosion.frosty.top right", allImages[1315]);
		namesToImages.put("explosion.frosty.middle left", allImages[1316]);
		namesToImages.put("explosion.frosty.middle center", allImages[1317]);
		namesToImages.put("explosion.frosty.middle right", allImages[1318]);
		namesToImages.put("explosion.frosty.bottom left", allImages[1319]);
		namesToImages.put("explosion.frosty.bottom center", allImages[1320]);
		namesToImages.put("explosion.frosty.bottom right", allImages[1321]);
		namesToImages.put("zap.magic missile.vertical", allImages[1322]);
		namesToImages.put("zap.magic missile.horizontal", allImages[1323]);
		namesToImages.put("zap.magic missile.left slant", allImages[1324]);
		namesToImages.put("zap.magic missile.right slant", allImages[1325]);
		namesToImages.put("zap.fire.vertical", allImages[1326]);
		namesToImages.put("zap.fire.horizontal", allImages[1327]);
		namesToImages.put("zap.fire.left slant", allImages[1328]);
		namesToImages.put("zap.fire.right slant", allImages[1329]);
		namesToImages.put("zap.cold.vertical", allImages[1330]);
		namesToImages.put("zap.cold.horizontal", allImages[1331]);
		namesToImages.put("zap.cold.left slant", allImages[1332]);
		namesToImages.put("zap.cold.right slant", allImages[1333]);
		namesToImages.put("zap.sleep.vertical", allImages[1334]);
		namesToImages.put("zap.sleep.horizontal", allImages[1335]);
		namesToImages.put("zap.sleep.left slant", allImages[1336]);
		namesToImages.put("zap.sleep.right slant", allImages[1337]);
		namesToImages.put("zap.death.vertical", allImages[1338]);
		namesToImages.put("zap.death.horizontal", allImages[1339]);
		namesToImages.put("zap.death.left slant", allImages[1340]);
		namesToImages.put("zap.death.right slant", allImages[1341]);
		namesToImages.put("zap.lightning.vertical", allImages[1342]);
		namesToImages.put("zap.lightning.horizontal", allImages[1343]);
		namesToImages.put("zap.lightning.left slant", allImages[1344]);
		namesToImages.put("zap.lightning.right slant", allImages[1345]);
		namesToImages.put("zap.poison gas.vertical", allImages[1346]);
		namesToImages.put("zap.poison gas.horizontal", allImages[1347]);
		namesToImages.put("zap.poison gas.left slant", allImages[1348]);
		namesToImages.put("zap.poison gas.right slant", allImages[1349]);
		namesToImages.put("zap.acid.vertical", allImages[1350]);
		namesToImages.put("zap.acid.horizontal", allImages[1351]);
		namesToImages.put("zap.acid.left slant", allImages[1352]);
		namesToImages.put("zap.acid.right slant", allImages[1353]);
		namesToImages.put("warning.0", allImages[1354]);
		namesToImages.put("warning.1", allImages[1355]);
		namesToImages.put("warning.2", allImages[1356]);
		namesToImages.put("warning.3", allImages[1357]);
		namesToImages.put("warning.4", allImages[1358]);
		namesToImages.put("warning.5", allImages[1359]);
		namesToImages.put("cmap.wall.vertical.mine", allImages[1360]);
		namesToImages.put("cmap.wall.horizontal.mine", allImages[1361]);
		namesToImages.put("cmap.wall.top left corner.mine", allImages[1362]);
		namesToImages.put("cmap.wall.top right corner.mine", allImages[1363]);
		namesToImages.put("cmap.wall.bottom left corner.mine", allImages[1364]);
		namesToImages
				.put("cmap.wall.bottom right corner.mine", allImages[1365]);
		namesToImages.put("cmap.wall.crosswall.mine", allImages[1366]);
		namesToImages.put("cmap.wall.tee up.mine", allImages[1367]);
		namesToImages.put("cmap.wall.tee down.mine", allImages[1368]);
		namesToImages.put("cmap.wall.tee left.mine", allImages[1369]);
		namesToImages.put("cmap.wall.tee right.mine", allImages[1370]);
		namesToImages.put("cmap.wall.vertical.gehennom", allImages[1371]);
		namesToImages.put("cmap.wall.horizontal.gehennom", allImages[1372]);
		namesToImages
				.put("cmap.wall.top left corner.gehennom", allImages[1373]);
		namesToImages.put("cmap.wall.top right corner.gehennom",
				allImages[1374]);
		namesToImages.put("cmap.wall.bottom left corner.gehennom",
				allImages[1375]);
		namesToImages.put("cmap.wall.bottom right corner.gehennom",
				allImages[1376]);
		namesToImages.put("cmap.wall.crosswall.gehennom", allImages[1377]);
		namesToImages.put("cmap.wall.tee up.gehennom", allImages[1378]);
		namesToImages.put("cmap.wall.tee down.gehennom", allImages[1379]);
		namesToImages.put("cmap.wall.tee left.gehennom", allImages[1380]);
		namesToImages.put("cmap.wall.tee right.gehennom", allImages[1381]);
		namesToImages.put("cmap.wall.vertical.knox", allImages[1382]);
		namesToImages.put("cmap.wall.horizontal.knox", allImages[1383]);
		namesToImages.put("cmap.wall.top left corner.knox", allImages[1384]);
		namesToImages.put("cmap.wall.top right corner.knox", allImages[1385]);
		namesToImages.put("cmap.wall.bottom left corner.knox", allImages[1386]);
		namesToImages
				.put("cmap.wall.bottom right corner.knox", allImages[1387]);
		namesToImages.put("cmap.wall.crosswall.knox", allImages[1388]);
		namesToImages.put("cmap.wall.tee up.knox", allImages[1389]);
		namesToImages.put("cmap.wall.tee down.knox", allImages[1390]);
		namesToImages.put("cmap.wall.tee left.knox", allImages[1391]);
		namesToImages.put("cmap.wall.tee right.knox", allImages[1392]);
		namesToImages.put("cmap.wall.vertical.sokoban", allImages[1393]);
		namesToImages.put("cmap.wall.horizontal.sokoban", allImages[1394]);
		namesToImages.put("cmap.wall.top left corner.sokoban", allImages[1395]);
		namesToImages
				.put("cmap.wall.top right corner.sokoban", allImages[1396]);
		namesToImages.put("cmap.wall.bottom left corner.sokoban",
				allImages[1397]);
		namesToImages.put("cmap.wall.bottom right corner.sokoban",
				allImages[1398]);
		namesToImages.put("cmap.wall.crosswall.sokoban", allImages[1399]);
		namesToImages.put("cmap.wall.tee up.sokoban", allImages[1400]);
		namesToImages.put("cmap.wall.tee down.sokoban", allImages[1401]);
		namesToImages.put("cmap.wall.tee left.sokoban", allImages[1402]);
		namesToImages.put("cmap.wall.tee right.sokoban", allImages[1403]);
		namesToImages.put("UNUSED", allImages[1404]);
		namesToImages.put("UNUSED", allImages[1405]);
		namesToImages.put("monster.angelic being.baku", allImages[1406]);
		namesToImages.put("monster.humanoid.scathe seer", allImages[1407]);
		namesToImages.put("monster.human or elf.grey elf", allImages[1408]);
		namesToImages.put("monster.human or elf.drow", allImages[1409]);
		namesToImages.put("monster.human or elf.elf", allImages[1410]);
		namesToImages.put("UNUSED", allImages[1411]);
		namesToImages.put("UNUSED", allImages[1412]);
		namesToImages.put("UNUSED", allImages[1413]);
		namesToImages.put("UNUSED", allImages[1414]);
		namesToImages.put("UNUSED", allImages[1415]);
		namesToImages.put("UNUSED", allImages[1416]);
		namesToImages.put("UNUSED", allImages[1417]);
		namesToImages.put("UNUSED", allImages[1418]);
		namesToImages.put("UNUSED", allImages[1419]);
		namesToImages.put("UNUSED", allImages[1420]);
		namesToImages.put("UNUSED", allImages[1421]);
		namesToImages.put("UNUSED", allImages[1422]);
		namesToImages.put("UNUSED", allImages[1423]);
		namesToImages.put("UNUSED", allImages[1424]);
		namesToImages.put("UNUSED", allImages[1425]);
		namesToImages.put("UNUSED", allImages[1426]);
		namesToImages.put("UNUSED", allImages[1427]);
		namesToImages.put("UNUSED", allImages[1428]);
		namesToImages.put("UNUSED", allImages[1429]);
		namesToImages.put("UNUSED", allImages[1430]);
		namesToImages.put("UNUSED", allImages[1431]);
		namesToImages.put("UNUSED", allImages[1432]);
		namesToImages.put("UNUSED", allImages[1433]);
		namesToImages.put("UNUSED", allImages[1434]);
		namesToImages.put("UNUSED", allImages[1435]);
		namesToImages.put("UNUSED", allImages[1436]);
		namesToImages.put("UNUSED", allImages[1437]);
		namesToImages.put("UNUSED", allImages[1438]);
		namesToImages.put("UNUSED", allImages[1439]);
		namesToImages.put("system.tile.front", allImages[1440]);
		namesToImages.put("system.tile.back", allImages[1441]);
		namesToImages.put("system.tile.highlight", allImages[1442]);
		namesToImages.put("system.unit.friendly", allImages[1443]);
		return namesToImages;
	}
}
