package net.myexperiments.viciouscycle.util;

import java.util.Random;

public class MapMaker
{
    private static int[][][] allBoxes = new int[][][]
        {
            {
                { 19, 19, 19 },
                { 19, 3, 2 },
                { 19, 1, 19 } },
            {
                { 19, 19, 19 },
                { 2, 4, 19 },
                { 19, 1, 19 } },
            {
                { 19, 1, 19 },
                { 19, 5, 2 },
                { 19, 19, 19 } },
            {
                { 19, 1, 19 },
                { 2, 6, 19 },
                { 19, 19, 19 } },
            {
                { 19, 1, 19 },
                { 2, 7, 2 },
                { 19, 1, 19 } },
            {
                { 19, 1, 19 },
                { 2, 8, 2 },
                { 19, 19, 19 } },
            {
                { 19, 19, 19 },
                { 2, 9, 2 },
                { 19, 1, 19 } },
            {
                { 19, 1, 19 },
                { 2, 10, 19 },
                { 19, 1, 19 } },
            {
                { 19, 1, 19 },
                { 19, 11, 2 },
                { 19, 1, 19 } } };
    private static Random rand = new Random();
    
    public static int[][] randomMap(int numRows, int numCols)
    {
        int[][][][] currentMapBoxes = new int[(numRows + 2) / 3][(numCols + 2) / 3][3][3];
        int[][] currentMap = new int[numRows][numCols];
        int idx = 0;
        for (int r = 0; r < (numRows + 2) / 3; r++)
        {
            for (int c = 0; c < (numCols + 2) / 3; c++)
            {
                idx = rand.nextInt(allBoxes.length);
                currentMapBoxes[r][c] = allBoxes[idx];
            }
            
        }
        
        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numCols; j++)
            {
                currentMap[i][j] = currentMapBoxes[i / 3][j / 3][i % 3][j % 3];
            }
        }
        return currentMap;
    }
    
    private static int[][][] allLargerBoxes = new int[][][]
        {
            {
                { 19, 19, 19, 19, 19 },
                { 19, 19, 19, 19, 19 },
                { 19, 19, 3, 2, 2 },
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 } },
            {
                { 19, 19, 19, 19, 19 },
                { 19, 19, 19, 19, 19 },
                { 2, 2, 4, 19, 19 },
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 } },
            {
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 },
                { 19, 19, 5, 2, 2 },
                { 19, 19, 19, 19, 19 },
                { 19, 19, 19, 19, 19 }, },
            {
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 },
                { 2, 2, 6, 19, 19 },
                { 19, 19, 19, 19, 19 },
                { 19, 19, 19, 19, 19 } },
            {
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 },
                { 2, 2, 7, 2, 2 },
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 } },
            {
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 },
                { 2, 2, 8, 2, 2 },
                { 19, 19, 19, 19, 19 },
                { 19, 19, 19, 19, 19 } },
            {
                { 19, 19, 19, 19, 19 },
                { 19, 19, 19, 19, 19 },
                { 2, 2, 9, 2, 2 },
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 } },
            {
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 },
                { 2, 2, 10, 19, 19 },
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 } },
            {
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 },
                { 19, 19, 11, 2, 2 },
                { 19, 19, 1, 19, 19 },
                { 19, 19, 1, 19, 19 } } };
    
    public static int[][] randomLargeRoomsMap(int numRows, int numCols)
    {
        int[][][][] currentMapBoxes = new int[(numRows + 4) / 5][(numCols + 4) / 5][5][5];
        int[][] currentMap = new int[numRows][numCols];
        int idx = 0;
        for (int r = 0; r < (numRows + 4) / 5; r++)
        {
            for (int c = 0; c < (numCols + 4) / 5; c++)
            {
                idx = rand.nextInt(allLargerBoxes.length);
                currentMapBoxes[r][c] = allLargerBoxes[idx];
            }
            
        }
        
        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numCols; j++)
            {
                currentMap[i][j] = currentMapBoxes[i / 5][j / 5][i % 5][j % 5];
            }
        }
        return currentMap;
    }
    
    private static int[][][] allGeomorphComponents = new int[][][]
        {
            {
                { 21, 1, 19, 1, 21 },
                { 2, 7, 2, 8, 2 },
                { 19, 1, 19, 19, 19 },
                { 2, 10, 19, 3, 2, },
                { 21, 1, 19, 1, 21 } },
            {
                { 21, 1, 19, 1, 21 },
                { 2, 6, 19, 5, 2 },
                { 19, 19, 19, 19, 19 },
                { 2, 4, 19, 3, 2, },
                { 21, 1, 19, 1, 21 } },
            {
                { 21, 1, 19, 1, 21 },
                { 2, 8, 2, 8, 2 },
                { 19, 19, 19, 19, 19 },
                { 2, 4, 19, 3, 2, },
                { 21, 1, 19, 1, 21 } } };
    
    private static int[][][] allLargeGeomorphs = new int[][][]
        {
            {
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 },
                { 2, 7, 2, 8, 2, 2, 8, 2, 8, 2 },
                { 19, 1, 19, 19, 19, 19, 19, 19, 19, 19 },
                { 2, 10, 19, 19, 19, 19, 19, 19, 3, 2, },
                { 21, 1, 19, 19, 19, 19, 19, 19, 1, 21 },
                { 21, 1, 19, 19, 19, 19, 19, 19, 1, 21 },
                { 2, 10, 19, 19, 19, 19, 19, 19, 5, 2, },
                { 19, 1, 19, 19, 19, 19, 19, 19, 19, 19 },
                { 2, 10, 19, 3, 2, 2, 4, 19, 3, 2 },
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 } },
            {
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 },
                { 2, 6, 19, 5, 2, 2, 6, 19, 5, 2 },
                { 19, 19, 19, 19, 19, 19, 19, 19, 19, 19 },
                { 2, 4, 19, 19, 19, 19, 19, 19, 3, 2, },
                { 21, 1, 19, 19, 19, 19, 19, 19, 1, 21 },
                { 21, 1, 19, 19, 19, 19, 19, 19, 1, 21 },
                { 2, 6, 19, 19, 19, 19, 19, 19, 5, 2, },
                { 19, 19, 19, 19, 19, 19, 19, 19, 19, 19 },
                { 2, 4, 19, 3, 2, 2, 4, 19, 3, 2 },
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 } },
            {
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 },
                { 2, 8, 2, 8, 2, 2, 8, 2, 8, 2 },
                { 19, 19, 19, 19, 19, 19, 19, 19, 19, 19 },
                { 2, 4, 19, 19, 19, 19, 19, 19, 3, 2, },
                { 21, 1, 19, 19, 19, 19, 19, 19, 1, 21 },
                { 21, 1, 19, 19, 19, 19, 19, 19, 1, 21 },
                { 2, 6, 19, 19, 19, 19, 19, 19, 5, 2, },
                { 19, 19, 19, 19, 19, 19, 19, 19, 19, 19 },
                { 2, 4, 19, 3, 2, 2, 4, 19, 3, 2 },
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 } } };
    private static int[][][] allSpecialGeomorphs = new int[][][]
        {
            {
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 },
                { 2, 7, 2, 8, 2, 2, 8, 2, 7, 2 },
                { 19, 1, 19, 19, 19, 19, 19, 19, 1, 19 },
                { 2, 10, 19, 19, 19, 19, 19, 19, 11, 2, },
                { 21, 1, 19, 19, 19, 28, 19, 19, 1, 21 },
                { 21, 1, 19, 19, 19, 19, 19, 19, 1, 21 },
                { 2, 10, 19, 19, 19, 19, 19, 19, 11, 2, },
                { 19, 1, 19, 19, 19, 19, 19, 19, 1, 19 },
                { 2, 10, 19, 3, 2, 2, 4, 19, 11, 2 },
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 } },
            {
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 },
                { 2, 6, 19, 5, 2, 2, 6, 19, 5, 2 },
                { 19, 19, 19, 32, 32, 32, 32, 19, 19, 19 },
                { 2, 4, 32, 32, 32, 32, 32, 32, 3, 2, },
                { 21, 1, 32, 32, 32, 32, 32, 32, 1, 21 },
                { 21, 1, 32, 32, 32, 32, 32, 32, 1, 21 },
                { 2, 6, 32, 32, 32, 32, 32, 32, 5, 2, },
                { 19, 19, 19, 32, 32, 32, 32, 19, 19, 19 },
                { 2, 4, 19, 3, 2, 2, 4, 19, 3, 2 },
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 } },
            {
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 },
                { 2, 6, 19, 5, 2, 2, 6, 19, 5, 2 },
                { 19, 19, 19, 32, 32, 32, 32, 19, 19, 19 },
                { 2, 4, 19, 32, 32, 32, 32, 32, 3, 2, },
                { 21, 1, 19, 32, 32, 32, 32, 32, 1, 21 },
                { 21, 1, 19, 32, 32, 32, 32, 32, 1, 21 },
                { 2, 6, 19, 32, 32, 32, 32, 32, 5, 2, },
                { 19, 19, 19, 19, 19, 19, 19, 19, 19, 19 },
                { 2, 4, 19, 3, 2, 2, 4, 19, 3, 2 },
                { 21, 1, 19, 1, 21, 21, 1, 19, 1, 21 } } };
    
    /*
     * new int[][] { new int[] { 19, 1, 21, 1, 19, 19, 1, 21, 1, 19 }, new int[]
     * { 2, 6, 21, 5, 2, 2, 6, 21, 5, 2 }, new int[] { 21, 21, 21, 21, 21, 21,
     * 21, 21, 21, 21 }, new int[] { 2, 4, 21, 21, 21, 21, 21, 21, 3, 2, }, new
     * int[] { 19, 1, 21, 21, 21, 21, 21, 21, 1, 19 }, new int[] { 19, 1, 21,
     * 21, 21, 21, 21, 21, 1, 19 }, new int[] { 2, 6, 21, 21, 21, 21, 21, 21, 5,
     * 2, }, new int[] { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 }, new int[] {
     * 2, 4, 21, 3, 2, 2, 4, 21, 3, 2 }, new int[] { 19, 1, 21, 1, 19, 19, 1,
     * 21, 1, 19 } }, new int[][] { new int[] { 19, 1, 21, 1, 19, 19, 1, 21, 1,
     * 19 }, new int[] { 2, 8, 2, 8, 2, 2, 8, 2, 8, 2 }, new int[] { 21, 21, 21,
     * 21, 21, 21, 21, 21, 21, 21 }, new int[] { 2, 4, 21, 21, 21, 21, 21, 21,
     * 3, 2, }, new int[] { 19, 1, 21, 21, 21, 21, 21, 21, 1, 19 }, new int[] {
     * 19, 1, 21, 21, 21, 21, 21, 21, 1, 19 }, new int[] { 2, 6, 21, 21, 21, 21,
     * 21, 21, 5, 2, }, new int[] { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 },
     * new int[] { 2, 4, 21, 3, 2, 2, 4, 21, 3, 2 }, new int[] { 19, 1, 21, 1,
     * 19, 19, 1, 21, 1, 19 } } };
     */
    
    public static int[][] rotateGeomorphCW(int[][] gm)
    {
        int[][] curr = new int[gm[0].length][gm.length], curr2 = new int[gm[0].length][gm.length];
        for (int i = 0; i < gm.length; i++)
        {
            for (int j = 0; j < gm[0].length; j++)
            {
                curr[j][(gm.length - 1) - i] = gm[i][j];
            }
        }
        
        for (int i = 0; i < curr.length; i++)
        {
            for (int j = 0; j < curr[0].length; j++)
            {
                switch (curr[i][j])
                {
                case 1:
                    curr2[i][j] = 2;
                    break;
                case 2:
                    curr2[i][j] = 1;
                    break;
                case 3:
                    curr2[i][j] = 4;
                    break;
                case 4:
                    curr2[i][j] = 6;
                    break;
                case 5:
                    curr2[i][j] = 3;
                    break;
                case 6:
                    curr2[i][j] = 5;
                    break;
                case 8:
                    curr2[i][j] = 11;
                    break;
                case 9:
                    curr2[i][j] = 10;
                    break;
                case 10:
                    curr2[i][j] = 8;
                    break;
                case 11:
                    curr2[i][j] = 9;
                    break;
                case 13:
                    curr2[i][j] = 14;
                    break;
                case 14:
                    curr2[i][j] = 13;
                    break;
                case 15:
                    curr2[i][j] = 16;
                    break;
                case 16:
                    curr2[i][j] = 15;
                    break;
                default:
                    curr2[i][j] = curr[i][j];
                }
            }
        }
        return curr2;
    }
    
    public static int[][] randomGeomorphsMap(int numRows, int numCols)
    {
        int[][][][] currentMapBoxes = new int[(numRows + 9) / 10][(numCols + 9) / 10][10][10];
        int[][] currentMap = new int[numRows][numCols];
        boolean[][] currentRoomPositions = new boolean[numRows / 5 + 1][numCols / 5 + 1];
        int[][] currentFusedComponents = new int[10][10];
        int idx = 0;
        int[][] currComponent = new int[5][5];
        
        for (int i = 0; i < currentRoomPositions.length; i++)
        {
            for (int j = 0; j < currentRoomPositions[i].length; j++)
            {
                currentRoomPositions[i][j] = true;
            }
        }
        for (int i = 0; i < currentRoomPositions.length; i++)
        {
            currentRoomPositions[i][0] = false;
            currentRoomPositions[i][currentRoomPositions[0].length - 1] = false;
        }
        for (int i = 0; i < currentRoomPositions[0].length; i++)
        {
            currentRoomPositions[0][i] = false;
            currentRoomPositions[currentRoomPositions.length - 1][i] = false;
        }
        
        for (int r = 0; r < (numRows + 9) / 10; r++)
        {
            for (int c = 0; c < (numCols + 9) / 10; c++)
            {
                if (rand.nextInt(9) < 4)
                {
                    idx = rand.nextInt(allLargeGeomorphs.length);
                    currentMapBoxes[r][c] = allLargeGeomorphs[idx];
                    for (int i = rand.nextInt(4); i < 3; i++)
                    {
                        currentMapBoxes[r][c] = rotateGeomorphCW(currentMapBoxes[r][c]);
                    }
                    currentRoomPositions[r * 2 + 1][c * 2 + 1] = false;
                } else if (rand.nextInt(11) < 2)
                {
                    idx = rand.nextInt(allSpecialGeomorphs.length);
                    currentMapBoxes[r][c] = allSpecialGeomorphs[idx];
                    for (int i = rand.nextInt(4); i < 3; i++)
                    {
                        currentMapBoxes[r][c] = rotateGeomorphCW(currentMapBoxes[r][c]);
                    }
                    currentRoomPositions[r * 2 + 1][c * 2 + 1] = false;
                } else
                {
                    currentFusedComponents = new int[10][10];
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            idx = rand.nextInt(allGeomorphComponents.length);
                            currComponent = allGeomorphComponents[idx];
                            
                            for (int q = rand.nextInt(4); q < 3; q++)
                            {
                                currComponent = rotateGeomorphCW(currComponent);
                            }
                            for (int k = 0; k < 5; k++)
                            {
                                for (int l = 0; l < 5; l++)
                                {
                                    currentFusedComponents[(i * 5) + k][(j * 5)
                                            + l] = currComponent[k][l];
                                    
                                }
                            }
                        }
                    }
                    currentMapBoxes[r][c] = currentFusedComponents;
                }
                
            }
            
        }
        
        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numCols; j++)
            {
                currentMap[i][j] = currentMapBoxes[i / 10][j / 10][i % 10][j % 10];
            }
        }
        currentMap = createDoors(currentMap, currentRoomPositions);
        currentMap = ensurePassages(currentMap);
        switch (rand.nextInt(11))
        {
        case 0:
        case 1:
        case 2:
        case 3:
            break;
        case 4:
            currentMap = makePurpleWalls(currentMap);
            break;
        case 5:
            currentMap = makeLavaWalls(currentMap); // makePaleWalls(currentMap);
            break;
        case 6:
            currentMap = makeGoldWalls(currentMap);
            break;
        case 7:
            currentMap = makeMineWalls(currentMap);
            break;
        case 8:
            currentMap = makeGreenWalls(currentMap);
            break;
        case 9:
            currentMap = makeIceWalls(currentMap);
            break;
        case 10:
            currentMap = makeLavaWalls(currentMap);
            break;
        }
        return currentMap;
    }
    
    private static int[][] ensurePassages(int[][] currentMap)
    {
        // TODO Auto-generated method stub
        int firstWallPos = -1, secondWallPos = -1;
        // boolean[] sideWalls;
        for (int i = 3; i < currentMap.length; i++)
        {
            if (i % 5 == 3)
            {
                for (int j = 3; j < currentMap[i].length; j++)
                {
                    if (j % 5 == 3)
                    {
                        if (currentMap[i - 1][j] == 1)
                        {
                            firstWallPos = j;
                        } else
                        {
                            firstWallPos = -1;
                        }
                    }
                    if (j % 5 == 1)
                    {
                        if (currentMap[i - 1][j] == 1)
                        {
                            secondWallPos = j;
                        } else
                        {
                            secondWallPos = -1;
                        }
                    }
                    
                    // if(j % 5 == 2) { firstWallPos = -1; secondWallPos = -1; }
                    
                    if (firstWallPos >= 3 && secondWallPos >= 6)
                    {
                        currentMap[i - 1][firstWallPos] = 15;
                        currentMap[i - 1][secondWallPos] = 15;
                    }
                }
            }
        }
        firstWallPos = -1;
        secondWallPos = -1;
        for (int i = 3; i < currentMap[0].length; i++)
        {
            if (i % 5 == 3)
            {
                for (int j = 3; j < currentMap.length; j++)
                {
                    if (j % 5 == 3)
                    {
                        if (currentMap[j][i - 1] == 2)
                        {
                            firstWallPos = j;
                        } else
                        {
                            firstWallPos = -1;
                        }
                    }
                    if (j % 5 == 1)
                    {
                        if (currentMap[j][i - 1] == 2)
                        {
                            secondWallPos = j;
                        } else
                        {
                            secondWallPos = -1;
                        }
                    }
                    /*
                     * if (j % 5 == 2) { firstWallPos = -1; secondWallPos = -1;
                     * }
                     */
                    if (firstWallPos >= 3 && secondWallPos >= 6)
                    {
                        currentMap[firstWallPos][i - 1] = 16;
                        currentMap[secondWallPos][i - 1] = 16;
                    }
                }
            }
        }
        return currentMap;
    }
    
    private static int[][][] allGeomorphDoorComponents = new int[][][]
        {
            {
                { 3, 2, 2, 4 },
                { 15, 19, 19, 15 },
                { 1, 19, 19, 1 },
                { 5, 2, 2, 6 } },
            {
                { 3, 2, 16, 4 },
                { 15, 19, 19, 1 },
                { 1, 19, 19, 1 },
                { 5, 2, 2, 6 } },
            {
                { 3, 2, 2, 4 },
                { 15, 19, 19, 1 },
                { 1, 19, 19, 15 },
                { 5, 2, 2, 6 } },
            {
                { 3, 16, 2, 4 },
                { 15, 19, 19, 1 },
                { 1, 19, 19, 1 },
                { 5, 2, 2, 6 } },
            {
                { 3, 2, 16, 4 },
                { 15, 19, 19, 15 },
                { 1, 19, 19, 1 },
                { 5, 2, 2, 6 } },
            {
                { 3, 2, 16, 4 },
                { 15, 19, 19, 15 },
                { 1, 19, 19, 1 },
                { 5, 2, 16, 6 } } };
    
    private static int[][] createDoors(int[][] currentMap,
            boolean[][] roomPositions)
    {
        // int[][][][] currentDoorBoxes = new
        // int[roomPositions.length][roomPositions[0].length][4][4];
        int idx;
        int[][] currentRoomComponent = new int[4][4];
        for (int i = 1; i < roomPositions.length - 1; i++)
        {
            for (int j = 1; j < roomPositions[i].length - 1; j++)
            {
                if (roomPositions[i][j] == true && rand.nextInt(8) < 2)
                {
                    idx = rand.nextInt(allGeomorphComponents.length);
                    currentRoomComponent = allGeomorphDoorComponents[idx];
                    
                    for (int q = rand.nextInt(4); q < 3; q++)
                    {
                        currentRoomComponent = rotateGeomorphCW(currentRoomComponent);
                    }
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            if (!((k == 0 && l == 0) || (k == 0 && l == 3)
                                    || (k == 3 && l == 0) || (k == 3 && l == 3)))
                                currentMap[i * 5 - 2 + k][j * 5 - 2 + l] = currentRoomComponent[k][l];
                        }
                    }
                }
            }
        }
        return currentMap;
    }
    

    public static int[][] randomMegaGeomorphsMap(int numRows, int numCols)
    {
        int[][][][] currentMapBoxes = new int[(numRows + 13) / 14][(numCols + 13) / 14][14][14];
        int[][] currentMap = new int[numRows][numCols];
        boolean[][] currentRoomPositions = new boolean[numRows / 7 + 2][numCols / 7 + 2];
        int[][] currentFusedComponents = new int[14][14];
        int idx = 0;
        int[][] currComponent = new int[7][7];
        
        for (int i = 0; i < currentRoomPositions.length; i++)
        {
            for (int j = 0; j < currentRoomPositions[i].length; j++)
            {
                currentRoomPositions[i][j] = true;
            }
        }
        for (int i = 0; i < currentRoomPositions.length; i++)
        {
            currentRoomPositions[i][0] = false;
            currentRoomPositions[i][currentRoomPositions[0].length - 1] = false;
            currentRoomPositions[i][currentRoomPositions[0].length - 2] = false;
        }
        for (int i = 0; i < currentRoomPositions[0].length; i++)
        {
            currentRoomPositions[0][i] = false;
            currentRoomPositions[currentRoomPositions.length - 1][i] = false;
            currentRoomPositions[currentRoomPositions.length - 2][i] = false;
        }
        
        for (int r = 0; r < (numRows + 13) / 14; r++)
        {
            for (int c = 0; c < (numCols + 13) / 14; c++)
            {
                if (rand.nextInt(9) < 4)
                {
                    idx = rand.nextInt(MegaGeomorphs.allLargeGeomorphs.length);
                    currentMapBoxes[r][c] = MegaGeomorphs.allLargeGeomorphs[idx];
                    for (int i = rand.nextInt(4); i < 3; i++)
                    {
                        currentMapBoxes[r][c] = rotateGeomorphCW(currentMapBoxes[r][c]);
                    }
                    currentRoomPositions[r * 2 + 1][c * 2 + 1] = false;
                } else if (rand.nextInt(11) < 2)
                {
                    idx = rand.nextInt(MegaGeomorphs.allSpecialGeomorphs.length);
                    currentMapBoxes[r][c] = MegaGeomorphs.allSpecialGeomorphs[idx];
                    for (int i = rand.nextInt(4); i < 3; i++)
                    {
                        currentMapBoxes[r][c] = rotateGeomorphCW(currentMapBoxes[r][c]);
                    }
                    currentRoomPositions[r * 2 + 1][c * 2 + 1] = false;
                } else
                {
                    currentFusedComponents = new int[14][14];
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            idx = rand.nextInt(MegaGeomorphs.allGeomorphComponents.length);
                            currComponent = MegaGeomorphs.allGeomorphComponents[idx];
                            
                            for (int q = rand.nextInt(4); q < 3; q++)
                            {
                                currComponent = rotateGeomorphCW(currComponent);
                            }
                            for (int k = 0; k < 7; k++)
                            {
                                for (int l = 0; l < 7; l++)
                                {
                                    currentFusedComponents[(i * 7) + k][(j * 7)
                                            + l] = currComponent[k][l];
                                    
                                }
                            }
                        }
                    }
                    currentMapBoxes[r][c] = currentFusedComponents;
                }
                
            }
            
        }
        
        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numCols; j++)
            {
                currentMap[i][j] = currentMapBoxes[i / 14][j / 14][i % 14][j % 14];
            }
        }
        currentMap = createMegaDoors(currentMap, currentRoomPositions);
        currentMap = ensureMegaPassages(currentMap);
        switch (rand.nextInt(15))
        {
        case 0:
        case 1:
        case 2:
        case 3:
            break;
        case 4:
            currentMap = makePurpleWalls(currentMap);
            break;
        case 5:
            currentMap = makeMineWalls(currentMap); // makePaleWalls(currentMap);
            break;
        case 6:
            currentMap = makeGoldWalls(currentMap);
            break;
        case 7:
            currentMap = makeMineWalls(currentMap);
            break;
        case 8:
            currentMap = makeGreenWalls(currentMap);
            break;
        case 9:
            currentMap = makeIceWalls(currentMap);
            break;
        case 10:
            currentMap = makeLavaWalls(currentMap);
            break;
        case 11:
            currentMap = makeWateryWalls(currentMap);
        break;
        case 12:
            currentMap = makeTrippyWalls(currentMap);
        break;
        case 13:
            currentMap = makeBrickWalls(currentMap);
        break;
        case 14:
            break;
        
        }
        return currentMap;
    }
    
    private static int[][] ensureMegaPassages(int[][] currentMap)
    {
        // TODO Auto-generated method stub
        int firstWallPos = -1, secondWallPos = -1;
        // boolean[] sideWalls;
        for (int i = 5; i < currentMap.length; i++)
        {
            if (i % 7 == 5)
            {
                for (int j = 5; j < currentMap[i].length; j++)
                {
                    if (j % 7 == 5)
                    {
                        if (currentMap[i - 2][j] == 1)
                        {
                            firstWallPos = j;
                        } else
                        {
                            firstWallPos = -1;
                        }
                    }
                    if (j % 7 == 1)
                    {
                        if (currentMap[i - 2][j] == 1)
                        {
                            secondWallPos = j;
                        } else
                        {
                            secondWallPos = -1;
                        }
                    }
                    
                    // if(j % 5 == 2) { firstWallPos = -1; secondWallPos = -1; }
                    
                    if (firstWallPos >= 5 && secondWallPos >= 8)
                    {
                        currentMap[i - 2][firstWallPos] = 15;
                        currentMap[i - 2][secondWallPos] = 15;
                    }
                }
            }
        }
        firstWallPos = -1;
        secondWallPos = -1;
        for (int i = 3; i < currentMap[0].length; i++)
        {
            if (i % 7 == 5)
            {
                for (int j = 5; j < currentMap.length; j++)
                {
                    if (j % 7 == 5)
                    {
                        if (currentMap[j][i - 2] == 2)
                        {
                            firstWallPos = j;
                        } else
                        {
                            firstWallPos = -1;
                        }
                    }
                    if (j % 7 == 1)
                    {
                        if (currentMap[j][i - 2] == 2)
                        {
                            secondWallPos = j;
                        } else
                        {
                            secondWallPos = -1;
                        }
                    }
                    /*
                     * if (j % 5 == 2) { firstWallPos = -1; secondWallPos = -1;
                     * }
                     */
                    if (firstWallPos >= 5 && secondWallPos >= 8)
                    {
                        currentMap[firstWallPos][i - 2] = 16;
                        currentMap[secondWallPos][i - 2] = 16;
                    }
                }
            }
        }
        return currentMap;
    }
    
   
    
    private static int[][] createMegaDoors(int[][] currentMap,
            boolean[][] roomPositions)
    {
        // int[][][][] currentDoorBoxes = new
        // int[roomPositions.length][roomPositions[0].length][4][4];
        int idx;
        int[][] currentRoomComponent = new int[4][4];
        for (int i = 1; i < roomPositions.length - 1; i++)
        {
            for (int j = 1; j < roomPositions[i].length - 1; j++)
            {
                if (roomPositions[i][j] == true && rand.nextInt(8) < 2)
                {
                    idx = rand.nextInt(MegaGeomorphs.allGeomorphDoorComponents.length);
                    currentRoomComponent = MegaGeomorphs.allGeomorphDoorComponents[idx];
                    
                    for (int q = rand.nextInt(4); q < 3; q++)
                    {
                        currentRoomComponent = rotateGeomorphCW(currentRoomComponent);
                    }
                    for (int k = 0; k < 4; k++)
                    {
                        for (int l = 0; l < 4; l++)
                        {
                            if (!((k == 0 && l == 0) || (k == 0 && l == 3)
                                    || (k == 3 && l == 0) || (k == 3 && l == 3)))
                                currentMap[i * 7 - 2 + k][j * 7 - 2 + l] = currentRoomComponent[k][l];
                        }
                    }
                }
            }
        }
        return currentMap;
    }
    
    
    
    
    
    public static int[][] makeMineWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 184;
                }/*
                  * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                  * currentMap[i][j] = 19; }
                  */
            }
        }
        return currentMap;
    }
    
    public static int[][] makePurpleWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 195;
                }/*
                  * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                  * currentMap[i][j] = 19; }
                  */
            }
        }
        return currentMap;
    }
    
    public static int[][] makePaleWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 206;
                }/*
                  * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                  * currentMap[i][j] = 19; }
                  */
            }
        }
        return currentMap;
    }
    
    public static int[][] makeGoldWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 217;
                }/*
                  * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                  * currentMap[i][j] = 19; }
                  */
            }
        }
        return currentMap;
    }
    
    public static int[][] makeGreenWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 268;
                }/*
                  * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                  * currentMap[i][j] = 19; }
                  */
            }
        }
        return currentMap;
    }
    
    public static int[][] makeIceWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 279;
                }/*
                  * if(currentMap[i][j] == 32) { currentMap[i][j] = 33; }
                  * 
                  * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                  * currentMap[i][j] = 19; }
                  */
            }
        }
        return currentMap;
    }

    public static int[][] makeLavaWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 290;
                }
                if (currentMap[i][j] == 32)
                {
                    currentMap[i][j] = 34;
                }
                /*
                 * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                 * currentMap[i][j] = 19; }
                 */
            }
        }
        return currentMap;
    }
    public static int[][] makeWateryWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 306;
                }/*
                if (currentMap[i][j] == 32)
                {
                    currentMap[i][j] = 34;
                }
                /*
                 * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                 * currentMap[i][j] = 19; }
                 */
            }
        }
        return currentMap;
    }
    public static int[][] makeTrippyWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 317;
                }
                if (currentMap[i][j] == 32)
                {
                    currentMap[i][j] = 56;
                }
                /*
                 * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                 * currentMap[i][j] = 19; }
                 */
            }
        }
        return currentMap;
    }
    public static int[][] makeBrickWalls(int[][] currentMap)
    {
        for (int i = 0; i < currentMap.length; i++)
        {
            for (int j = 0; j < currentMap[i].length; j++)
            {
                if (currentMap[i][j] > 0 && currentMap[i][j] <= 11)
                {
                    currentMap[i][j] = currentMap[i][j] + 328;
                }/*
                if (currentMap[i][j] == 32)
                {
                    currentMap[i][j] = 34;
                }
                /*
                 * if(currentMap[i][j] >= 13 && currentMap[i][j] <= 16) {
                 * currentMap[i][j] = 19; }
                 */
            }
        }
        return currentMap;
    }
}

/*
 * 
 * (defn corners [] (take 10 (shuffle (concat (repeat 15 [[19 19 19] [19 3 2]
 * [19 1 19]]) (repeat 15 [[19 19 19] [2 4 19] [19 1 19]]) (repeat 15 [[19 1 19]
 * [19 5 2] [19 19 19]]) (repeat 15 [[19 1 19] [2 6 19] [19 19 19]]) (repeat 15
 * [[19 1 19] [2 7 2] [19 1 19]]) (repeat 15 [[19 1 19] [2 8 2] [19 19 19]])
 * (repeat 15 [[19 19 19] [2 9 2] [19 1 19]]) (repeat 15 [[19 1 19] [2 10 19]
 * [19 1 19]]) (repeat 15 [[19 1 19] [19 11 2] [19 1 19]])))))
 */
