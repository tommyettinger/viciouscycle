package net.myexperiments.viciouscycle.controller;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;

import net.myexperiments.gos.GOSSystem;
import net.myexperiments.gos.World;
import net.myexperiments.viciouscycle.VCGame;

public class VCGOS {
public World world;
public VCGame gtge;

public VCGOS() throws IOException {

	boolean append = true;
    FileHandler handler = new FileHandler("gos.log", append);
    GOSSystem.getLogger().addHandler(handler);
    GOSSystem.getLogger().addHandler(new ConsoleHandler());
    	

/**
* 
* 		Setup table names
 * @return 
* 
* 
*/

}
public void InitGOS(VCGame view) throws IOException{

world = new World();
world.CurrentTurn = 0;
gtge = view;

//TheGame.SystemConfig = new Properties();
//TheGame.SystemConfig.load(new FileInputStream(GOSSystem.CONFIGDIRECTORY
//	+ File.separatorChar + "SysConfig.properties"));


world.InitWorld();

}

public void update() {
	world.GOSPS.SendMail();
	world.scheduler.EventLoop();
}


}
